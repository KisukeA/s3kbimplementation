import express from "express";
const app = express()
import userRoutes from "./routes/users.js";
import authRoutes from "./routes/auth.js";
import commentRoutes from "./routes/comments.js";
import likesRoutes from "./routes/likes.js";
import postRoutes from "./routes/posts.js";
import locationRoutes from "./routes/locations.js";
import subjectRoutes from "./routes/subject.js";
import searchRoutes from "./routes/search.js";
import contractRoutes from "./routes/contract.js";
import dotenv from 'dotenv';
dotenv.config();
import cors from "cors";
import multer from "multer";
import cookieParser from "cookie-parser";
import path from "path";
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "../client/public/upload");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({ storage: storage });

const port = 5006 

const corsOptions = {
    origin: 'http://88.200.63.148:3006', // Specify the requesting origin
    credentials: true, // Accept credentials (cookies, authentication data)
  };

const config = {
  devServer: {
    watchOptions: {
      ignored: [
        'client/public/upload' // Use an absolute or relative path as needed
      ]
    }
  }
};

app.use(cors(corsOptions))
app.use(cookieParser())
app.use(express.json())
app.use(express.static(path.join(__dirname, "build")));

app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname,"build", "index.html"))
})

app.post("/server/upload", upload.single("profile"), (req, res) => {
  const file = req.file;
  console.log(file.filename);
  return res.status(200).json(file.filename);
});

//seperate routers for different functionalities
app.use("/server/users",userRoutes) 
app.use("/server/posts",postRoutes)
app.use("/server/comments",commentRoutes)
app.use("/server/likes",likesRoutes)
app.use("/server/locations",locationRoutes)
app.use("/server/subject",subjectRoutes)
app.use("/server/search",searchRoutes)
app.use("/server/contract",contractRoutes)
app.use("/server/auth",authRoutes)

///App listening on port
app.listen(process.env.PORT || port, ()=>{
console.log(`Server is running on port: ${process.env.PORT || port}`)
})

export default config;