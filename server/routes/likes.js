import express from "express";
import { getLikes, deleteLike, addLike } from "../controllers/likes.js";

const router = express.Router()

router.get("/:postId", getLikes);
router.post("/", addLike);
router.delete("/:postId", deleteLike);

export default router;