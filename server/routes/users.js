import express from "express";
import { getUser,getUserRole, getUserRatings, updateUser } from "../controllers/users.js";

const router = express.Router()

//for each functionality, there's a respective controller that handles the functions for each endpoint
router.get("/get/:userId",getUser);
router.get("/role/:userId",getUserRole);
router.get("/rating/:userId",getUserRatings);
router.put("/", updateUser)

export default router;