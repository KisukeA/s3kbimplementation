import express from "express";
import { getPosts, addPost, deletePost, getSinglePost } from "../controllers/posts.js";

const router = express.Router()

router.get("/", getPosts);
router.get("/singlepost/:postId/:authorId", getSinglePost);
router.post("/", addPost);
router.delete("/:id", deletePost);

export default router;