import express from "express";
import { getContracts, getUserContracts, getMyContracts, deleteContract, addContract, updateContract, getCounterContracts, getPendingContracts, getNotiContracts, getNoti2Contracts } from "../controllers/contracts.js";

const router = express.Router()

router.get("/", getContracts);
router.get("/my", getMyContracts);
router.get("/user", getUserContracts);
router.get("/counter", getCounterContracts);
router.get("/pending", getPendingContracts);
router.get("/noti", getNotiContracts);
router.get("/noti2", getNoti2Contracts); 
router.post("/", addContract);
router.put("/", updateContract);
router.delete("/:id", deleteContract);

export default router;