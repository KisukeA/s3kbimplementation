import express from "express";
import { getUserResults, getKeywordResults, getSubjectResults } from "../controllers/search.js";

const router = express.Router()

router.get("/users/:username", getUserResults);
router.get("/keywords/:keyword", getKeywordResults);
router.get("/subject/:subject", getSubjectResults);

export default router;