import express from "express";
import { getLocations, getUserLocations, deleteLocations, addLocations } from "../controllers/locations.js";

const router = express.Router()

router.get("/", getLocations);
router.get("/:userId", getUserLocations);
router.post("/", addLocations);
router.delete("/", deleteLocations);

export default router;