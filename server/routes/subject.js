import express from "express";
import { getSubjects, deleteSubject, addSubjects, getSomeSubjects, getAllSubjects } from "../controllers/subject.js";

const router = express.Router()

//get andd all have to be before :others because it causes problems otherwise
//it matches them as well and takes get or all to be the value of others
router.get("/get", getSomeSubjects);
router.get("/all", getAllSubjects);
router.get("/:others", getSubjects);
router.post("/", addSubjects);
router.delete("/", deleteSubject);

export default router;