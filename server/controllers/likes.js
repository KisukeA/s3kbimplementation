import { db } from "../conn.js"
import jwt from "jsonwebtoken";

export const addLike = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "INSERT INTO Likes (`user_id`,`post_id`) VALUES (?) ";
        const values = [
            userInfo.id,
            req.body.postId
        ]
        return db.query(q,[values], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json("Like added");
        })
    });
}
export const getLikes = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const postId = req.params.postId;
        const q = `
            SELECT user_id FROM Likes WHERE post_id = ?
        `;
        return db.query(q, [postId], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data.map(like=>like.user_id));
        });
    });    
}
export const deleteLike = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "DELETE FROM Likes WHERE `post_id` = ? AND `user_id` = ?";
        return db.query(q,[req.params.postId,userInfo.id], (err,data)=>{
            if(err) return res.status(500).json(err)
            if(data.affectedRows>0) return res.status(200).json("Like deleted");
            return res.status(403).json("Cannot delete that like");
        })
    });
}