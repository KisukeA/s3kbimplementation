import { db } from "../conn.js"
import bcrypt from "bcryptjs"
import jwt from "jsonwebtoken"

export const login = (req,res) =>{

    const q = "SELECT * FROM User WHERE username = ?"
    db.query(q,[req.body.username],(err,data)=>{
        if(err) return res.status(500).json(err)
        if(!data.length) return res.status(404).json("User not found")

        const checkPass = bcrypt.compareSync(req.body.password, data[0].password)
        if(!checkPass) return res.status(400).json("Incorrect password or username");
        const token = jwt.sign({id:data[0].id}, "genryuusaishigekuniyamamoto");
        const { password, ...others } = data[0];
        res.cookie("token",token,{
            httpOnly:true,
        }).status(200).json(others);
    }); 
}

export const logout = (req,res) =>{
    return res.clearCookie("token",{
        httpOnly:true,
    }).status(200).json("User has been logged out")
}

export const register = (req,res) =>{
    //simple authentication for now no hashing no security
    const q = "SELECT * FROM User WHERE username = ?"
    db.query(q,[req.body.username], (err,data)=>{
        if(err) return res.status(500).json(err)
        if(data.length) {return res.status(409).json("User already exists")}
        else{
            //hashing password and saving that
            if(req.body.password != req.body.confirm){return res.status(400).json("Passwords do not match")}
            
            const salt = bcrypt.genSaltSync(10);
            const hashedPass = bcrypt.hashSync(req.body.password, salt)
    
            const q1 = "INSERT INTO User (`username`, `password`, `role`, `nickname`) VALUE (?)"
            const values = [req.body.username, hashedPass, req.body.role, req.body.nickname]

            db.query(q1,[values], (err,data)=>{
                if(err) return res.status(500).json(err)
                else return res.status(200).json("User created")
            } )
        }
    })
}