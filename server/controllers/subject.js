import { db } from "../conn.js"
import jwt from "jsonwebtoken"


export const addSubjects = (req,res) =>{

  const token = req.cookies.token;
  if (!token) return res.status(401).json("Not authenticated!");

  jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
    db.beginTransaction(err => {
      if (err) {
        return res.status(500).json(err);
      }
      const subjects = req.body.rows;//we named the data rows when we sent a req
      console.log(subjects);
      //we will store the subject id's here
      let insertedSubjectIds = [];
      //array of promises, for each new (id = '') subject we create a promis
      const insertSubjectPromises = subjects.filter(sub => sub.id==="").map(sub => 
        new Promise((resolve, reject) => {
          //for each new subject we insert it in the db
          const query = 'INSERT INTO Subject (name, year, cycle, faculty) VALUES (?, ?, ?, ?)';
          db.query(query, [sub.name, sub.year, sub.level, sub.faculty], (err, data) => {
            if (err) reject(err);
            else resolve(data.insertId); // Capture the inserted subject ID
          });
        })
      );
      //creates one promise that resolves when all of the promises in the array resolve
      Promise.all(insertSubjectPromises).then(subjectIds => {
        //we get an array with the resolution of all promises as result
        insertedSubjectIds = subjectIds; // Array of IDs of newly inserted subjects
    
        // Now, prepare to insert into User_Subject table
        const userSubjectValuesNew = subjectIds.map(insertedId => [userInfo.id, insertedId]);
        // For old subjects, directly use their sub.id
        const userSubjectValuesOld = subjects.filter(sub => sub.id!=="").map(sub => [userInfo.id, parseInt(sub.id,10)]);
        // Combine the two arrays
        const allUserSubjectValues = [...userSubjectValuesNew, ...userSubjectValuesOld];
        console.log(allUserSubjectValues);
        const userSubjectPlaceholders = allUserSubjectValues.map(() => '(?, ?)').join(', ');
        const insertUserSubjectQuery = `INSERT INTO User_Subject (u_id, s_id) VALUES ${userSubjectPlaceholders}`;
        db.query(insertUserSubjectQuery, [].concat(...allUserSubjectValues), (err, data) => {
          if (err) {
            return db.rollback(() => {
              res.status(500).json(err);
            });
          } else {
            db.commit(err => {
              if (err) {
                return db.rollback(() => {
                  res.status(500).json(err);
                });
              }
              res.status(200).json(data);
            });
          }
        });
      }).catch(err => {
        db.rollback(() => {
          res.status(500).json(err);
        });
      });
      /*
      //if its new we put it else we dont include it
      const values = subjects.map(sub => sub.id===""?[sub.name, sub.year, sub.level, sub.faculty]:[]);
      //for each subject we add this placeholder ?'s and join them into 1 string with , as delimiter
      const placeholders = subjects.map((sub) => sub.id===""?'(?, ?, ?, ?)':'').join(', ');
      //if new we put it else not
      const q = `INSERT INTO Subject (name, year, cycle, faculty) VALUES ${placeholders}`;
      //spread the valus array and concat them into one single array
      db.query(q, [].concat(...values), (err, data) => {
        if (err) {
        
            res.status(500).json(err);
        
        }
        const userSubjectValues = subjects.map(sid => [userInfo.id, sid]);
        const userSubjectPlaceholders = userSubjectValues.map(() => '(?, ?)').join(', ');
        const insertUserSubjectQuery = `INSERT INTO User_Subject (u_id, s_id) VALUES ${userSubjectPlaceholders}`;
        //res.status(200).json(data);
      });*/
    });
  })
}
export const addExistingSubjects = (req, res) =>{
  
}
export const getAllSubjects = (req, res) => {
  const token = req.cookies.token;
  if (!token) return res.status(401).json("Not authenticated!");
  jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
    const q = "SELECT * FROM Subject";
    db.query(
      q,(err, data) => {
          if(err) return res.status(500).json(err)
          return res.status(200).json(data);
      }
    );
  });
};
export const getSomeSubjects = (req, res) =>{
  // i could omit getting subjects which are already picked by the user (maybe i will in the future)
  //but if i do that, user can enter it as a new, with the same name, and we wouldnt stop it (in the frontend)
  //because we don't have all the subjects to check whether it's new or not
  //to achieve both we will have to omit the subjects in the front end not here
  const token = req.cookies.token;
    if (!token) return res.status(401).json("Not authenticated!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      const q = "SELECT id,name FROM Subject WHERE `faculty` = ? AND `year` = ? AND `cycle` = ?";
      db.query(
        q,[req.query.faculty, req.query.year, req.query.cycle],(err, data) => {
            if(err) return res.status(500).json(err)
            return res.status(200).json(data);
        }
      );
    });
}
export const getSubjects = (req, res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not authenticated!");
    const others = req.params.others==="true";
    console.log(others)
    console.log(req.params.others)
    console.log(req.query.userId)
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      const q = "SELECT s.*,us.rating FROM `Subject` s JOIN `User_Subject` us ON s.id = us.s_id AND us.u_id = ?";
      db.query(
        //we fetch others subjects or the users
        q,[others?req.query.userId:userInfo.id],(err, data) => {
            if(err) return res.status(500).json(err)
            return res.status(200).json(data);
        }
      );
    });
  };

export const deleteSubject = (req,res) => {
  const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");

        const ids = req.body.ids;
        if (!ids || ids.length === 0) return res.status(400).json("No IDs provided.");

        let placeholders = ids.map(() => '?').join(',');
        const query = `DELETE FROM Location WHERE id IN (${placeholders}) AND u_id = ?`;

        return db.query(query, [...ids, userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            if (data.affectedRows > 0) return res.status(200).json("Locations deleted.");
            return res.status(403).json("Cannot delete locations.");
        });
    });
}