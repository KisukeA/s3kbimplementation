import { db } from "../conn.js"
import jwt from "jsonwebtoken"

export const getUserLocations = (req,res) =>{

    const q = "SELECT * FROM Location WHERE u_id = ?";
      db.query(
        q,[req.params.userId],(err, data) => {
            if(err) return res.status(500).json(err)
            if(data.length === 0) return res.status(404).json("No locations found for this user!")
            return res.status(200).json(data);
        }
      );
}
export const addLocations = (req,res) =>{

  const token = req.cookies.token;
  if (!token) return res.status(401).json("Not authenticated!");

  jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");

    //const locations = req.body.rows;
    //it should be rows but for some reason its data.ids???

    const locations = req.body.rows;
    const values = locations.map(loc => [loc.placeName, loc.latitude, loc.longitude, userInfo.id]);
    const placeholders = locations.map(() => '(?, ?, ?, ?)').join(', ');
    const q = `INSERT INTO Location (placeName, latitude, longitude, u_id) VALUES ${placeholders}`;
    //we spread all subarrays in values and concat all of them in a single array containing all elements
    db.query(q, [].concat(...values), (err, data) => {
      if (err) {
          return res.status(500).json(err);
      }
      res.status(200).json(data);
    });
  })
}
export const getLocations = (req, res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not authenticated!");
  
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      //so, if nick not empty we include it in the update otherwise we omit it and just change pp
      const q = "SELECT * FROM Location WHERE u_id = ?";
      db.query(
        q,[userInfo.id],(err, data) => {
            if(err) return res.status(500).json(err)
            return res.status(200).json(data);
        }
      );
    });
  };

export const deleteLocations = (req,res) => {
  const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");

        // Assuming the request body contains an array of IDs named 'ids'
        const ids = req.body.ids;
        if (!ids || ids.length === 0) return res.status(400).json("No IDs provided.");

        // Construct a placeholder string for SQL query
        let placeholders = ids.map(() => '?').join(',');
        const query = `DELETE FROM Location WHERE id IN (${placeholders}) AND u_id = ?`;

        // Execute the query with the array of IDs and the user ID
        return db.query(query, [...ids, userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            if (data.affectedRows > 0) return res.status(200).json("Locations deleted.");
            return res.status(403).json("Cannot delete locations.");
        });
    });
}