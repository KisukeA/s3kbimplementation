import { db } from "../conn.js";
import jwt from "jsonwebtoken";
import moment from "moment";

export const getPosts = (req,res) =>{
    //get the boolean from the url, if true we're on profile page
    //and we fetch only our posts
    const profile = req.query.profile === 'true';
    const userProfile = req.query.userProfile;
    //getting the id of the user from the cookie we signed with the id
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid");
        //select every post where author is different than current user (timeline)
        const q1 = "SELECT p.*, s.name, username, profilePicture, role FROM Post AS p JOIN User AS u ON (u.id = p.author_id) JOIN Subject AS s ON (s.id=p.about) WHERE p.author_id != ? ORDER BY p.created DESC";
        const q2 = "SELECT p.*, s.name, username, profilePicture, role FROM Post AS p JOIN User AS u ON (u.id = p.author_id) JOIN Subject AS s ON (s.id=p.about) WHERE p.author_id = ? ORDER BY p.created DESC";
        if(profile){ //fetch our posts else others'
            return db.query(q2,[userInfo.id], (err,data)=>{
                if(err) return res.status(500).json(err);
                return res.status(200).json(data);
        })}
        if(userProfile != "null"){//fetch the users posts on their profile
            return db.query(q2,[userProfile], (err,data)=>{
                if(err) return res.status(500).json(err);
                return res.status(200).json(data);
        })}
        //else fetch every post (timeline/homepage)
        return db.query(q1,[userInfo.id], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    })
}

export const addPost = (req,res) => {
    const token = req.cookies.token
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "INSERT INTO Post (`author_id`,`content`,`created`,`keywords`,`type`,`about`) VALUES (?) ";
        const values = [
            userInfo.id,
            req.body.content,
            moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"),//time of post creation
            req.body.keywords,
            req.body.type,
            req.body.about
        ]
        return db.query(q,[values], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json("Post created");
        })
    });

}
export const deletePost = (req,res) => {
    const token = req.cookies.token
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");

        const q = "DELETE FROM Post WHERE `id` = ? AND `author_id` = ?";
        return db.query(q,[req.params.id,userInfo.id], (err,data)=>{
            if(err) return res.status(500).json(err);
            if(data.affectedRows>0) return res.status(200).json("Post deleted");
            return res.status(403).json("Cannot delete that post");
        })
    });

}
export const getSinglePost = (req,res) => {
    const q = "SELECT s.name,p.*, username, profilePicture, nickname, role FROM Post AS p JOIN User AS u ON (u.id = p.author_id) JOIN Subject AS s ON (s.id=p.about) WHERE p.author_id = ? AND p.id = ?";
        return db.query(q,[req.params.authorId, req.params.postId], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json(data);
        })
}