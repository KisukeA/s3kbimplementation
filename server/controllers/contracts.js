import { db } from "../conn.js"
import jwt from "jsonwebtoken";
import moment from 'moment-timezone';

export const addContract = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "INSERT INTO Contract "
        +"(`offerer_id`,`receiver_id`,`location_id`,`post_id`,`amount`,`date`,`timeFrom`,`timeTo`,`status`,`subject_id`)"
        +"VALUES (?) ";
    //adding the hours to the timeFrom to get a timeTo string
    const time = req.body.timeFrom;
    const [hours, minutes] = time.split(':').map(Number);
    const newTime = new Date();
    newTime.setHours(parseInt(hours) + parseInt(req.body.hours), minutes, 0, 0);
    const newHours = newTime.getHours();
    const newMinutes = newTime.getMinutes();
    //created the minutes and hours that form timeTo
        const values = [
            userInfo.id,
            req.body.receiver_id,
            req.body.location_id,
            req.body.post_id,
            req.body.amount,
            req.body.date,
            req.body.timeFrom,
            `${newHours.toString().padStart(2, '0')}:${newMinutes.toString().padStart(2, '0')}`,
            //add padding so we standardize the time format (0's before single digit hours)
            req.body.status,
            req.body.subject_id
        ]
        return db.query(q,[values], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json("Contract added");
        })
    });
}
export const getNoti2Contracts = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const q = `
            SELECT c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
            WHERE (c.offerer_id = ? OR c.receiver_id = ?) AND 
            (c.status = "cancelled" OR c.status="approved" OR c.status="refused") 
            ORDER BY c.id DESC
            
        `;
        return db.query(q, [userInfo.id, userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    });    
}
export const getNotiContracts = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const q = `
            SELECT c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
            WHERE c.offerer_id = ? AND 
            (c.status="declined" OR c.status="accepted") 
            ORDER BY c.id DESC
            
        `;
        return db.query(q, [userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    });    
}
export const getMyContracts = (req,res) =>{
  const token = req.cookies.token;
  if (!token) return res.status(401).json("Not logged in!");
  jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      const q = `
          SELECT c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
          WHERE (c.receiver_id = ? OR c.offerer_id = ?) AND (c.status = "accepted" OR c.status = "seen") ORDER BY c.id DESC  
      `;
      //also get the "seen" contracts, they are notifications of contract proposals by the user
      //that have been accepted and came as a notification and user pressed dismiss notification,(user saw the notification)
      //i handled it this way instead of creating a new notification table
      return db.query(q, [userInfo.id,userInfo.id], (err, data) => {
          if (err) return res.status(500).json(err);
          return res.status(200).json(data);
      });
  });    
}
export const getContracts = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const q = `
            SELECT c.*, u.username ,u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
            WHERE c.receiver_id = ? AND c.status = "pending" ORDER BY c.id DESC
            
        `;
        return db.query(q, [userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    });    
}
export const getPendingContracts = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const q = `
            SELECT c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
            WHERE c.offerer_id = ? AND c.status = "pending" ORDER BY c.id DESC
            
        `;
        return db.query(q, [userInfo.id], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    });    
}
export const getCounterContracts = (req,res) =>{
  const token = req.cookies.token;
  if (!token) return res.status(401).json("Not logged in!");
  jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      const q = `
          SELECT c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id
          WHERE c.receiver_id = ? AND c.status = "counter" ORDER BY c.id DESC
          
      `;
      return db.query(q, [userInfo.id, userInfo.id], (err, data) => {
          if (err) return res.status(500).json(err);
          return res.status(200).json(data);
      });
  });    
}
export const deleteContract = (req,res) =>{
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "DELETE FROM Contract WHERE id = ?";
        return db.query(q,[req.params.id], (err,data)=>{
            if(err) return res.status(500).json(err)
            if(data.affectedRows>0) return res.status(200).json("Contract deleted");
            return res.status(403).json("Cannot delete that contract");
        })
    });
}
export const updateContract = (req, res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not authenticated!");
  
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      if(req.body.counter){//if we are making a counter offer
        const time = req.body.timeFrom;
        const [hours, minutes] = time.split(':').map(Number);
        const newTime = new Date();
        newTime.setHours(parseInt(hours) + parseInt(req.body.hours), minutes, 0, 0);
        const newHours = newTime.getHours();
        const newMinutes = newTime.getMinutes();
        const values = [
          userInfo.id,
          req.body.location_id,
          req.body.receiver_id,
          req.body.amount,
          req.body.date.includes('T')?moment(req.body.date).tz("Europe/Ljubljana").format('YYYY-MM-DD'):req.body.date,
          req.body.timeFrom,
          `${newHours.toString().padStart(2, '0')}:${newMinutes.toString().padStart(2, '0')}`,
          req.body.status,
          req.body.id
        ]
        const q1 = "UPDATE Contract SET `offerer_id` = ?, `location_id` = ?, `receiver_id` = ?, `amount` = ?, `date` = ?, `timeFrom` = ?, `timeTo` = ?, `status` = ? WHERE id = ?";
        return db.query(
            q1,values,(err, data) => {
                if (err) return res.status(500).json(err);
                if (data.affectedRows > 0) return res.json("Updated!");
                return res.status(403).json("You can update only your contract!");
            }
            );  
        }
        //if we request cancel we enter here
        if(req.body.canceller){
            //we set the status to cancelled but also we need to know who requested cancel
            //so the other user is the only one who can answer the request
            const q = "UPDATE Contract SET `status`= ?, `canceller_id` = ? WHERE id = ?";
            console.log(q)
            return db.query(
              q,[req.body.status,userInfo.id, req.body.id],(err, data) => {
                if (err) return res.status(500).json(err);
                if (data.affectedRows > 0) return res.json("Updated!");
                return res.status(403).json("You can update only your contract!");
              }
            );
        }
        if(req.body.rating){
            const q = "UPDATE Contract SET `status`= ?, `rating` = ? WHERE id = ?";
            
            return db.query(
                q,[req.body.status,req.body.rating, req.body.id],(err, data) => {
                  if (err) return res.status(500).json(err);
                  if (data.affectedRows <= 0) return res.status(403).json("You can update only your contract")
                  const subjectId = req.body.subject_id;
                  // calculate the new average rating for the subject
                  const calculateAvgQuery = 'SELECT AVG(rating) AS average_rating FROM Contract WHERE subject_id = ?';
                  console.log(subjectId);      
                  db.query(calculateAvgQuery, [subjectId], (error, results) => {
                    if (error) return res.status(500).json(error);
                    console.log(results);
                    const averageRating = results[0].average_rating;
                    // ipdate the UserSubject table with the new average rating
                    const updateUserSubjectQuery = "UPDATE User_Subject SET `rating` = ? WHERE s_id = ? AND u_id = ?";
                     console.log(averageRating) 
                    db.query(updateUserSubjectQuery, [averageRating, subjectId, userInfo.id], (error, results) => {
                      if (error) return res.status(500).json(error);
                      return res.json("Updated User_Subject ratings");
                    });
                  });
                }
              );
        }

        //if we declined/accepted an offer or if we dismissed a notification (set status to upcoming)
      const q = "UPDATE Contract SET `status`= ? WHERE id = ?"
      return db.query(
        q,[req.body.status, req.body.id],(err, data) => {
          if (err) return res.status(500).json(err);
          if (data.affectedRows > 0) return res.json("Updated!");
          return res.status(403).json("You can update only your contract!");
        }
      );
    });
};
export const getUserContracts = (req,res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
        if (err) return res.status(403).json("Token is not valid!");
        const q = `
            SELECT l.placeName AS locationName, s.name, c.*, u.username, u1.username AS receiver_username, u.profilePicture FROM Contract c JOIN User u ON u.id = offerer_id JOIN User u1 ON u1.id = receiver_id JOIN Subject s ON s.id = c.subject_id JOIN Location l ON l.id = c.location_id
            WHERE (c.receiver_id = ? OR c.offerer_id = ?) AND (c.status = "accepted" OR c.status = "seen") ORDER BY c.id DESC  
        `;
        return db.query(q, [req.query.userId, req.query.userId], (err, data) => {
            if (err) return res.status(500).json(err);
            return res.status(200).json(data);
        });
    });  
}