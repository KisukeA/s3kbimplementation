import { db } from "../conn.js"
import jwt from "jsonwebtoken";

export const getUserResults = (req,res) =>{
    const username = req.params.username;
    if(username === ""){return res.status(200).json([]);}
    const q = "SELECT id, username, profilePicture, role, nickname FROM User WHERE username LIKE ?";
    return db.query(q,['%'+username+'%'],(err,data)=>{
        if(err) return res.status(500).json(err)
        return res.status(200).json(data);
    });
}
export const getKeywordResults = (req,res) =>{
    const keyword = req.params.keyword;
    if(keyword === ""){return res.status(200).json([]);}
    const q = "SELECT * FROM Post WHERE keywords LIKE ? ORDER BY created DESC";
    return db.query(q,['%#'+keyword+'%'],(err,data)=>{
        if(err) return res.status(500).json(err)
        return res.status(200).json(data);
    });
}
export const getSubjectResults = (req,res) => {
    const subject = req.params.subject;
    if(subject === ""){return res.status(200).json([]);}
    const q = "SELECT p.*,s.name as subjectName FROM Post p, Subject s WHERE p.about = s.id AND s.name LIKE ? ORDER BY p.created DESC";
    return db.query(q,['%'+subject+'%'],(err,data)=>{
        if(err) return res.status(500).json(err)
        return res.status(200).json(data);
    });
}