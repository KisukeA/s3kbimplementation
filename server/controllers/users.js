import { db } from "../conn.js"
import jwt from "jsonwebtoken";

export const getUser = (req,res) =>{
    const userId = req.params.userId;
    const q = "SELECT * FROM User WHERE id = ?";
    return db.query(q,[userId],(err,data)=>{
        if(err) return res.status(500).json(err)
        if(data.length === 0) return res.status(404).json("User not found")
        const { password, ...others } = data[0];
        return res.status(200).json(others);
    });
}
export const getUserRole = (req,res) =>{
  const userId = req.params.userId;
  const q = "SELECT role FROM User WHERE id = ?";
  return db.query(q,[userId],(err,data)=>{
      if(err) return res.status(500).json(err)
      return res.status(200).json(data);
  });
}
export const getUserRatings = (req,res) =>{
  const userId = req.params.userId;
    const q = "SELECT s.id,s.name, us.rating FROM Subject s, User_Subject us WHERE us.u_id = ? AND s.id = us.s_id";
    return db.query(q,[userId],(err,data)=>{
        if(err) return res.status(500).json(err)
        return res.status(200).json(data);
    });
}
export const updateUser = (req, res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not authenticated!");
  
    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
      if (err) return res.status(403).json("Token is not valid!");
      //for some reason i cant handle the logic in the frontend?? it automatically sends the data and refreshes?///
      //reason: as said in the request already, large files cause problems, and sending files 
      //automatically refreshes page even with preventDefault 
      //so, if nick not empty we include it in the update otherwise we omit it and just change pp
      const changeNick = req.body.nickname.length > 0;
      const q1 =
        "UPDATE User SET `nickname`=?,`profilePicture`=? WHERE id=? ";
      const q2 =
        "UPDATE User SET `profilePicture`=? WHERE id=? ";
      const values = [
        ...(changeNick ? [req.body.nickname] : []), // This will include the nickname only if changeNick is true
        req.body.profilePicture,
        userInfo.id,
      ];
      db.query(
        changeNick ? q1 : q2,
        values,
        (err, data) => {
          if (err) res.status(500).json(err);
          if (data.affectedRows > 0) return res.json("Updated!");
          return res.status(403).json("You can update only your post!");
        }
      );
    });
};