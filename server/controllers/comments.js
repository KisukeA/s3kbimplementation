import { db } from "../conn.js";
import jwt from "jsonwebtoken";
import moment from "moment";

export const getComments = (req,res) =>{
    const postId = req.query.postId;
    const q = `SELECT c.*, username, profilePicture, role 
    FROM Comment AS c JOIN User AS u ON (u.id = c.writer_id)
    WHERE c.post_id = ? ORDER BY c.created DESC`;
    return db.query(q,[postId], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json(data);
    });
}

export const addComment = (req,res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "INSERT INTO Comment (`writer_id`,`content`,`created`,`post_id`) VALUES (?) ";
        const values = [
            userInfo.id,
            req.body.content,
            moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"),//time of post creation
            req.body.postId
        ]
        return db.query(q,[values], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json("Comment added");
        })
    });
}
export const deleteComment = (req,res) => {
    const token = req.cookies.token;
    if (!token) return res.status(401).json("Not logged in!");

    jwt.verify(token, "genryuusaishigekuniyamamoto", (err, userInfo) => {
    if (err) return res.status(403).json("Token is not valid!");
        const q = "DELETE FROM Comment WHERE `id` = ? AND `writer_id` = ?";
        return db.query(q,[req.params.id,userInfo.id], (err,data)=>{
            if(err) return res.status(500).json(err);
            if(data.affectedRows>0) return res.status(200).json("Comment deleted");
            return res.status(403).json("Cannot delete that comment");
        })
    });
}