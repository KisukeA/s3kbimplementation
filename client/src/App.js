import { createBrowserRouter, RouterProvider, Outlet, Navigate, Link } from "react-router-dom";
import { AuthContext } from "./context/AuthContext.js";
import Login from "./pages/login.js";
import Register from "./pages/register.js";
import Profile from "./pages/profile.js";
import UserProfile from "./pages/userProfile.js";
import Home from "./pages/home.js";
import Land from "./components/Land.js";
import About from "./components/About.js";
import Contact from "./components/Contact.js";
import Nav from "./components/Nav.js";
import LeftBar from "./components/LeftBar.js";
import RightBar from "./components/RightBar.js";
import SinglePost from "./components/SinglePost.js";
import { useContext, useState } from "react";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query"
import './App.css';


function App() {

  function checkCookie(name) {
    function escape(s) { return s.replace(/([.*+?\^$(){}|\[\]\/\\])/g, '\\$1'); }
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : null;
}
  const queryClient = new QueryClient();

  const Layout = () => {
    return (
      <QueryClientProvider client = {queryClient}>
        <div style={{width:"100vw", height:"100vh" , overflow: "hidden" }} className="AppLayout">
          <Nav />
          <div style={{ display: "flex", justifyContent:"space-between", padding:"1vw", height:"90%" }}>
            <LeftBar />
            <div style={{ margin:"10px", width: "55%"}}>
              <Outlet />
            </div>
            <RightBar />
          </div>
        </div>
      </QueryClientProvider>
    );
  };

  const Landing = () => {
    const [activePage, setActivePage] = useState(null);
    console.log(activePage)
    return (
      <div className="container">
      <div className="left-section">
        {/* PAZI OVDE MOJT DA IMAT GRESKA */}
        <Link to="/landing" style={{ textDecoration: "none" }}>
          <div name="welcome" className={`navDiv ${activePage==="welcome"?'active':''}`} onClick={()=>{setActivePage('welcome')}} style={{'--is-top':'none'}}>
            Welcome</div>
        </Link>
        <Link to="/landing/about" style={{ textDecoration: "none" }}>
          <div name="about" className={`navDiv ${activePage==="about"?'active':''}`} onClick={()=>{setActivePage("about")}}>
            About</div>
        </Link>  
        <Link to="/landing/contact" style={{ textDecoration: "none" }}>
          <div name="contact" className={`navDiv ${activePage==="contact"?'active':''}`} onClick={()=>{setActivePage('contact')}}>
            Contact</div>
        </Link>
        <Link to="/login" style={{ textDecoration: "none" }}>
          <div className="navDiv">Login</div>
        </Link>
      </div>
      <div className="right-section">
          <Outlet />
      </div>
  </div>
    );
  };

  const { user } = useContext(AuthContext);

  const ProtectedRoute = ({ children }) => {
    if (!user) {
      return <Navigate to="/landing" />;
    }

    return children;
  };

  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <ProtectedRoute>
          <Layout />
        </ProtectedRoute>
      ),
      children: [
        {
          index:true,
          element: <Home />,
        },
        {
          path: "/profile/:id",
          element: <Profile />,
        },
        {
          path: "/userprofile/:userId",
          element: <UserProfile />,
        },
        {
          path: "/singlepost/:postId/:authorId",
          element: <SinglePost />,
        }
      ],
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/landing",
      element: <Landing />,
      children: [
        {
          index: true,
          element: <Land />,
        },
        {
          path: "about",
          element: <About />,
        },
        {
          path: "contact",
          element: <Contact />,
        }
      ]
    },
    {
      path: "/register",
      element: <Register />,
    },
  ]);

  return (
    <div style={{width: "100vw",height:"100vh",display: 'flex',  justifyContent:'center', alignItems:'center'}}>
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
