import { Link } from "react-router-dom"
import { useState, useEffect } from "react";
import "./register.css";
import axios from "axios";


const Register = () => {
    const [success, setSuccess] = useState(false);
    const [success2, setSuccess2] = useState(false);
    const [error,setError] = useState(null);
    const [info, setInfo] = useState({
        username: "",
        nickname: "",
        password: "",
        confirm: "",
        role:""
    })
    const updInfo = (e) =>{
        setInfo((previous)=>({...previous,[e.target.name]:e.target.value}));
        setSuccess2(false);
    }
    console.log(info);
    const sendRegister = async (e) => {
        setSuccess(false);
        e.preventDefault();
        if(info.username.length === 0 || info.confirm.length === 0 || info.password.length === 0 || info.role.length === 0){
            setError("Please fill all the fields");
            return;
        }
        try{
            const a = await axios.post("http://88.200.63.148:5006/server/auth/register", info);
            setSuccess2(true);
        }
        catch(err){
            console.log(err);
            setError(err.response.data);
        }
    }
    useEffect(() => {
        // Only set up a timer if there's an error
        if (error) {
            const timer = setTimeout(() => {
                setError(''); // Clear the error message
            }, 3000); // Set timeout for 3 seconds
    
            // Cleanup function to clear the timer
            return () => clearTimeout(timer);
        }
    }, [error]);
    /*useEffect(() => {
        // Only set up a timer if there's an error
        if (success) {
            const timer = setTimeout(() => {
                setSuccess(false); // Clear the error message
            }, 7000); // Set timeout for 7 seconds
    
            // Cleanup function to clear the timer
            return () => clearTimeout(timer);
            
        }
    }, [success]);*/
        return (
            <div>
                 <div className="register-container">
                        <Link to ="/"><span style={{display:"inline-block", marginBottom:"-30px", fontSize:"30px"}}>&#8678;</span></Link>
                    <h2 style={{margin:"5px"}}>Sign up</h2>
                    <form>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input style = {{margin:"0"}} onChange = {updInfo} type="text" id="username" name="username" placeholder="Enter your username" ></input>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input onChange = {updInfo} type="password" id="password" name="password" placeholder="Enter your password" ></input>
                        </div>
                        <div style={{marginBottom:"5px"}} className="form-group">
                            <label htmlFor="confirmPassword">Confirm Password</label>
                            <input onChange = {updInfo} type="password" id="confirmPassword" name="confirm" placeholder="Confirm your password" ></input>
                        </div>
                        
                        <div className = "roleButtons">
                            <div className="radio-group"style={{marginRight:"30px", alignSelf:"flex-start"}}>
                            <input
                              type="radio"
                              id="tutor"
                              name="role"
                              value="tutor"
                              onChange={updInfo}
                            />
                            <label htmlFor="tutor">tutor</label>
                            </div>
                            <div className="radio-group" style={{alignSelf:"flex-start", marginRight:"25px"}}>
                            <input
                              type="radio"
                              id="student"
                              name="role"
                              value="student"
                              onChange={updInfo}
                            />
                            <label htmlFor="student">student</label>
                            </div>
                        </div>
                        <div style = {{color:"red"}}>{error && error}</div>
                        {success2 && <div>Successfully created an account <Link style = {{textDecoration:"none"}} to = "/login">Log in.</Link></div>}
                        <div>Already have an account? <Link to ="/login">Log in.</Link></div>
                        <div>
                        </div>
                    </form>
                        {success && <div> 
                                        <div className = "popUp">
                                            <div className="popUpForm">
                                                <span>Last step, this is how you will appear to other users </span>
                                                <label htmlFor="nickname">Enter your nickname:</label>
                                                <div style = {{display:"flex", flexDirection:"row"}}>
                                                    <input id = "nickname" name="nickname" placeholder="Nickname..." type = "text" onChange={updInfo} style={{margin:"0"}}>
                                                    </input>
                                                    <button onClick={sendRegister}>Submit</button>
                                                </div>
                                                <button onClick={()=>setSuccess(false)} className="closePopUpButton">x</button>
                                            </div>
                                        </div> 
                                    </div>
                        }
                    <button onClick={() => setSuccess(true)} className="signupbtn">Sing up</button>
                </div>
            </div>
            )
        
}

export default Register


