import { Link, useNavigate } from "react-router-dom";
import { useState,useEffect } from "react";
import "./login.css";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext.js";

const Login = () => {
    const { login,setUser } = useContext(AuthContext);
    const navigate = useNavigate();
    const [error,setError] = useState(null);
    const [credentials, updateCredentials] = useState({
        username: "",
        password: ""
    });
    const sendCred = async (e) => {  
        e.preventDefault();
        if(credentials.username.length === 0 || credentials.password.length === 0){
            setError("Please fill all the fields");
            return;
        }
        try{
            const a = await login(credentials);
            const url = '/profile/' + a.data.id
            console.log(a);  
            navigate(url);
        }
        catch(err){
            console.log(err);
            setError(err.response?.data);
           }
    }
    const updateCred = (e) => {
        updateCredentials((previous)=>({...previous, [e.target.name]: e.target.value}));
    }
    useEffect(() => {
        // Only set up a timer if there's an error
        if (error) {
            const timer = setTimeout(() => {
                setError(''); // Clear the error message
            }, 3000); // Set timeout for 5 seconds
    
            // Cleanup function to clear the timer
            return () => clearTimeout(timer);
        }
    }, [error]);
    const handleClick = (req,res) => {
        // Your custom logic here
        setUser({id:0, username:"guest","profilePicture": null, role: "guest"})
      };
    //console.log(credentials);
    return (
        <div>
             <div className="login-container">
                    <Link to ="/"><span style={{display:"inline-block", marginBottom:"-30px", fontSize:"30px"}}>&#8678;</span></Link>
                <h2 style={{margin:"5px"}}>Login</h2>
                <form>
                    <div className="form-group">
                        <label htmlFor="username">Username:</label>
                        <input style={{marginBottom:"0px"}} onChange = {updateCred} className="loginInput"
                            type="text" id="username" name="username" placeholder="Enter your username"></input>
                    </div>
                    <div style={{marginBottom:"5px"}} className="form-group">
                        <label htmlFor="password">Password:</label>
                        <input onChange = {updateCred} type="password" id="password" className="loginInput"
                            name="password" placeholder="Enter your password"></input>
                    </div>
                    <div style = {{color:"red"}}>{error && error}</div>
                    <div>Don't have an account? <Link to ="/register">Sign up.</Link></div>
                    {/* <div>Or join as <Link  onClick={handleClick} to ="/profile/0">guest.</Link></div> */}
                    <div>
                        <button onClick={sendCred} className = "submitt" >Login</button>
                    </div>
                </form>
            </div>
        </div>
        )
}

export default Login
