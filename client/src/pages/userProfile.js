import { Link, useParams } from 'react-router-dom';
import { React, useState, useContext, useRef, useEffect } from "react";
import  Posts  from "../components/Posts.js";
import { makeRequest } from "../axios.js";
import { useQuery } from "@tanstack/react-query";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../context/AuthContext.js";
import Timetable from "../components/Timetable.js";
import "./userProfile.css"; 

const UserProfile = () => {
  const { user } = useContext(AuthContext);
 // Access URL parameters
  const { userId } = useParams();
  const pictureRef = useRef();
  const timetableRef = useRef();
  const [ timetableOpen, setTimetableOpen ] = useState(false);
  const [ picturePressed, setPicturePressed ] = useState(false);
  const [ ratingsOpen, setRatingsOpen ] = useState(false);
  //we use query since this data can be updated so we make sure we are up to date
  const { isLoading, error, data } = useQuery({
    queryKey:["userProfile", userId],
    queryFn: () =>{
      return makeRequest.get("/users/get/" + userId).then((res) => {
        console.log(res.data);
        return res.data;
      })
    }
  });
  const { isLoading:rIsLoading, error:rError, data:rData } = useQuery({
    queryKey:["userRating",userId],
    enabled: data?.role==="tutor",
    queryFn: () =>{
      return makeRequest.get("/users/rating/" + userId).then((res) => {
        console.log(res.data);
        return res.data;
      })
    }
  });
  const handleClickOutside = (event) => {
    if (pictureRef.current && !pictureRef.current.contains(event.target)) {
        setPicturePressed(false);
    }
  };
  useEffect(() => {
    if (picturePressed) {
      document.addEventListener('click', handleClickOutside, true);
    }
    return () => {
          document.removeEventListener('click', handleClickOutside, true);
    };
  }, [picturePressed]);
  const handleClickOutsideTT = (event) => {
    if (timetableRef.current && !timetableRef.current.contains(event.target)) {
        setTimetableOpen(false);
    }
  };
  useEffect(() => {
    if (timetableOpen) {
        document.addEventListener('mousedown', handleClickOutsideTT, true);
    }
  });
  return (
    <div className='profilePage'>
      {isLoading ? (
        "loading"
        ) : (
        <>
        <div className='profileNav'>
          <Link className = "aHomepage" to = "/"><button className='toHomePageButton'>Homepage</button></Link>
          <Link to = {`/profile/${user.id}`} className="aProfile"><button className="toProfilePageButton">Profile</button></Link>
          {data?.role==="tutor"&&<button onClick={()=>{setRatingsOpen(!ratingsOpen)}}className='ratingsButton'>Ratings</button>}
          {data?.role==="tutor"&&<button onClick={()=>{setTimetableOpen(true)}}className='ratingsButton'>Timetable</button>}

        </div>
        <div className = "profileInfo">
          <div style={{display:"flex", flexDirection:"row",justifyContent:"center", alignItems:"center"}}>
            <img onClick={()=>{setPicturePressed(true)}} style = {{margin:"0"}} src={`/upload/${!!data.profilePicture?data.profilePicture:"noPP.jpg"}`} className="userProfilePicture" />
            <h1 style={{padding:"10px", margin:"5px 0px", textAlign:"center"}}>User Profile Page</h1>
          </div>
          <div className='profileUserInfo'>
            <p>ID: {data.id}</p>  
            <p>Nickname: {data.nickname}</p>
            <p>username: {data.username}</p>
            <p>role: {data.role}</p>
          </div>
          {ratingsOpen && <><h3>Ratings</h3>
                {rIsLoading?"loading":rError?rError.message:
                <div className='ratingsContainer'>
                  {rData?.map((sub)=>(
                    <span key={sub.id}>{sub.name}: {sub.rating?sub.rating:"none"}</span>
                  ))}
                </div>}
              </>}
        </div>
        <div className='postsContainerProfile'>
          <Posts profile={false} userProfile = {userId}/>
        </div>
        </>
        )
        }
        {picturePressed && <div className='overlayPicture'>
          <img src={`/upload/${!!data.profilePicture?data.profilePicture:"noPP.jpg"}`} ref = {pictureRef} className="userPressedProfilePicture" />
        </div>}
        {timetableOpen && <Timetable user={data} timetableRef={timetableRef} />}
    </div>
    );
}

export default UserProfile;
