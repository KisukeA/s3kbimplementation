import { Link } from 'react-router-dom';
import { React, useState } from "react";
import  Posts  from "../components/Posts.js";
import Update from "../components/Update.js";
import { makeRequest } from "../axios.js";
import { useQuery } from "@tanstack/react-query";
import { useLocation } from "react-router-dom";
import "./profile.css";

const Profile = () => {
  const userId = parseInt(useLocation().pathname.split("/")[2]);
  //we use query since this data can be updated so we make sure we are up to date
  const [ ratingsOpen, setRatingsOpen ] = useState(false);
  const { isLoading, error, data } = useQuery({
    queryKey:["user"],
    queryFn: () =>{
      return makeRequest.get("/users/get/" + userId).then((res) => {
        console.log(res.data);
        return res.data;
      })
    }
  });
  //rating data
  const { isLoading:rIsLoading, error:rError, data:rData } = useQuery({
    queryKey:["rating"],
    enabled:data?.role==="tutor",
    queryFn: () =>{
      return makeRequest.get("/users/rating/" + userId).then((res) => {
        console.log(res.data);
        return res.data;
      })
    }
  });
  // Access URL parameters
  const [ update, setUpdateOpen ] = useState(false);
  return (
    <div className='profilePage'>
      {isLoading ? (
        "loading"
        ) : (
          <>
            <div className='profileNav'>
              <Link className = "aHomepage" to = "/"><button className='toHomePageButton'>Homepage</button></Link>
              {!update && <button onClick={()=>setUpdateOpen(true)}className='updateButton'>Update Profile</button>}
              {data?.role==="tutor" && <button onClick={()=>{setRatingsOpen(!ratingsOpen)}}className='ratingsButton'>Ratings</button>}
            </div>
            <div className = "profileInfo">
              <div style={{display:"flex", flexDirection:"row",justifyContent:"center", alignItems:"center"}}>
                <img style = {{margin:"0"}} src={`/upload/${data.profilePicture?data.profilePicture:"noPP.jpg"}`} 
                  className="userProfilePicture" />
                <h1 style={{padding:"10px", margin:"5px 0px", textAlign:"center"}}>Profile Page</h1>
              </div>
              <div className='profileUserInfo'>
                <p>ID: {data.id}</p>
                <p>Nickname: {data.nickname}</p>
                <p>username: {data.username}</p>
                <p>role: {data.role}</p>
              </div>
              
              {ratingsOpen && <><h3>Ratings</h3>
                {rIsLoading?"loading":rError?"rating error":
                <div className='ratingsContainer'>
                  {rData?.map((sub)=>(
                    <span key={sub.id}>{sub.name}: {sub.rating?sub.rating:"none"}</span>
                  ))}
                </div>}
              </>}
            </div>
            <div className='postsContainerProfile'>
              <Posts profile = {true} userProfile={null}/>
            </div>
            {update && <Update setUpdateOpen = {setUpdateOpen} user = {data}/>}
          </>
          )
        }
    </div>
    );
}

export default Profile;