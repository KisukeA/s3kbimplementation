import { Link } from "react-router-dom";
import { useEffect, useContext, useRef, useState } from "react";
import { AuthContext } from "../context/AuthContext.js";
import  Posts  from "../components/Posts.js";
import Share from "../components/Share.js";
import "./home.css";

const Home = () => {
    //track the homepage padding
    const element = document.querySelector('.homePage');
    let paddingTop = 0;
    if(element){ 
      const style = window.getComputedStyle(element);
      paddingTop = parseInt(style.paddingTop,10); 
    }
    const { user } = useContext(AuthContext);
    const [ isShareOpen, setIsShareOpen ] = useState(false);
    const [ shareNoti, setShareNoti ] = useState(false);
    const [ navHeight, setNavHeight ] = useState(0); //get and set the height of the nav (bcuz it could change responsively)
    const homeNavRef = useRef(null); //reference for the div which we set in the html
    const toggleShare = () => {
      setIsShareOpen(prev => !prev);
    };
    const [navWidth, setNavWidth] = useState('0px');
    //width of the home nav
    //we need to set it because of position:fixed, it doesn't respect the parents content size
    useEffect(() => {
    function updateWidth() {
      //getting the parent width and subtracting the padding
      const parent = document.querySelector('.homePage')
      const parentWidth = parent.offsetWidth;
      const style = window.getComputedStyle(parent);
      const paddingRight = parseFloat(style.paddingRight) + 10;
      setNavWidth(`${parentWidth-paddingRight}px`);
      //setting the width that our nav needs to be
    }
    //calling this function on resize
    window.addEventListener('resize', updateWidth);
    updateWidth(); // Initial setting
  
    return () => window.removeEventListener('resize', updateWidth);
    }, []); 
    useEffect(() => {
      if (shareNoti) {
          const timer = setTimeout(() => {
            setShareNoti(false);
          }, 5000);
          return () => clearTimeout(timer);
      }
    }, [shareNoti]);
    useEffect(() => {
      const resizeObserver = new ResizeObserver(entries => {
        for (let entry of entries) {
          //height of the div
          setNavHeight(entry.target.offsetHeight);
        }
      });
      //at the start it is null until we pass the reference to the div we need
      if (homeNavRef.current) {
        resizeObserver.observe(homeNavRef.current);//we observe our div
        //when it changes the above callback func is called changing the navheight variable
        //then react rerenders
      }
  
      // Clean up observer on component unmount
      return () => {
        if (homeNavRef.current) {
          resizeObserver.unobserve(homeNavRef.current);
        }
      };
    }, []);
    return (
      <div className="homePage">
        {/*we pass the reference here */}
        <div ref={homeNavRef} className = "homeNav" style={{ width: navWidth }}>
          <div style={{display: "inline-flex"}}>
            <Link to = {`/profile/${user.id}`} className="aProfile"><button className="toProfilePageButton">Profile</button></Link>
            <button className = "sharePostButton" style={{cursor:"pointer", borderRadius:"4px", border:"none"}} onClick={toggleShare}>{isShareOpen ? 'Close' : 'Share Post'}</button>
          </div>
          {isShareOpen && <Share setIsShareOpen={setIsShareOpen} setShareNoti = {setShareNoti}/>}
          <h1 style={{padding:"10px", margin:"5px 0px"}}>Timeline</h1>
        </div>
        {/*set the top margin equal to the nav height, and the height of the posts equal to full height - nav height*/}
        <div className="postsContainer" style={{height: `calc(100% - ${navHeight+paddingTop}px)`, marginTop:`${navHeight}px`}}>
          <Posts profile = {false} userProfile={null}/>
          {/*set whether we are on the profile page or home */}
          {/*so we know which posts to fetch (only user's or everybody else's)*/}
        </div>
        {shareNoti && <div className="shareNotification">
                  Post has been shared
                 </div>
        }
      </div>
    )
  }
  
  export default Home