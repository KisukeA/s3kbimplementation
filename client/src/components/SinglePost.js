import { useParams } from "react-router-dom";
import { makeRequest } from "../axios.js";
import { useQuery } from "@tanstack/react-query";
import Post from "./Post.js";

//now im realizing that we can do this another way by setting the clicked post in the minipost component
//and using that to display it somewhere but that would require passing the set function down some layers
//and probably reserving some space at some page
//maybe this solution as a seperate page is better
const SinglePost = () => {
    const { postId, authorId } = useParams();
    //take the post id and the author id from the url
    const { isLoading, error, data } = useQuery({
        queryKey:["post", postId],
        queryFn: () =>{
            //send the id's in the request to retrieve the correct post information
            //i could've just passed the whole post info to this component somehow but 
            //i think using query is the best because it's dynamic and if something changes
            //we will get the changes too (i think)
          return makeRequest.get("/posts/singlepost/" + postId +"/"+authorId).then((res) => {
            //data is returned as array of only 1 element since id is unique so we pass the first
            //element down in the post component
            return res.data;
          })
        }
      });
    return (
      <div className="SinglePost">
        {postId}
        sui 
        eseba
        {
            isLoading?"loading.."
            : error?error
            : <Post post={data[0]}/>
        }
      </div>
    )
  }
  
  export default SinglePost