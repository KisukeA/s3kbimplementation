import { useQuery } from "@tanstack/react-query";
import { makeRequest } from "../axios.js";
import { useState, useContext, useEffect } from "react";
import { AuthContext } from "../context/AuthContext.js";
import Post from "./Post.js";
import "./Posts.css";

const Posts = ({ profile, userProfile}) => {
  const { user } = useContext(AuthContext);
  const [ noti, setNoti ] = useState(false);
  const { isLoading, error, data } = useQuery({
      queryKey: ["posts", userProfile], 
      queryFn: () => {
        //doesn't work for guests, either i remove that functionality or make a workaround probably
        //because we don't have user or cookie and it is required before fetching data from db
      return makeRequest.get(`posts?profile=${profile}&userProfile=${userProfile}`).then((res)=>{
          return res.data;
      })}
      //we send the variable (whether we're in the profile or not to the api so we can fetch accordingly) 
  });
  useEffect(() => {
    if (noti) {
        const timer = setTimeout(() => {
          setNoti(false);
        }, 5000);
        return () => clearTimeout(timer);
    }
  }, [noti]);
    return (
        <div className="postsPosts" style={{padding: "10px",paddingBottom:"0", overflowY:"auto"}}>
          {error
              ? "Something went wrong!"
              : isLoading
              ? "loading"
              : data.length > 0 ? data.map((post) => <Post post={post} key={post.id} setNoti={setNoti} />)
              : "no posts"
          }
          {noti && <div className="postNotification">
                  Post has been deleted
                 </div>
          }
        </div>
    )
  };
  
  export default Posts;