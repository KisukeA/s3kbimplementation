import { useState, useEffect, useRef } from "react";
import { makeRequest } from "../axios.js";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import "./UpdateSubject.css";

const UpdateSubject = ({subjects, setSubjects}) => {

    const [ displayAddSubject, setDisplayAddSubject ] = useState(false);//open close add subject
    const [ displayTypeSubject, setdisplayTypeSubject ] = useState(false);//open close type field for subject
    const [ displayLevel, setDisplayLevel ] = useState(false);//open close level select
    const [ displaySelectSubject, setDisplaySelectSubject ] = useState(false);//open close selection for subject
    const [ error, setError ] = useState(null);
    const [ isConfirmButton, setIsConfirmButton ] = useState(true);//open close for confirm button +
    const [ displayRemoveSubject, setDisplayRemoveSubject ] = useState(false);
    const [ selectedRemoveSubjects, setSelectedRemoveSubjects ] = useState([]);
    const [ displayRemoveSubjectButton, setDisplayRemoveSubjectButton ] = useState(false);
    const [ subject, setSubject ] = useState({
        name:"",
        year:"",
        level:"",
        faculty:"",
        id:""
    })
    const subjectRef = useRef();
    const queryClient = useQueryClient();
    const { error:subjectError, isLoading, data:subjectData } = useQuery({
      queryKey:["subjects",subject],
      //this one with "s" the url is not
      queryFn: () => {
        if(!(subject.faculty !== "" && subject.level !=="" && subject.year!=="")) return [];
        return makeRequest.get("/subject/get", { params:{
          faculty: subject.faculty,
          cycle: subject.level,
          year: subject.year,
          }, }).then((res) => {
            return res.data;
          })
        }
    });
    useEffect(() => {
      console.log(subjectData)
      console.log(subject)
      //if the name we are typing is not contained in the subjects existing
      //then we can continue, else we say that it already exists and cant submit
      //or, if we are not typing but selecting directly from the options we can continue
      //because of course it will be contained there if we pick it from there
      if(!displayTypeSubject || !subjectData?.some(subjectElem => subjectElem.name === subject.name)){
        setError(null); return setIsConfirmButton(true);
      }
      setError("already exists!");
      return setIsConfirmButton(false);
    }, [subject,subjectData,displayTypeSubject]);
    const handleChange = (e) => {
        setSubject((previous)=>({...previous, [e.target.name]: e.target.value}));
    }
    
    const handleClickOutsideSubject = (event) => {
        if (subjectRef.current && !subjectRef.current.contains(event.target)) {
            setDisplayAddSubject(false);
        }
    };
    useEffect(() => {
        if (displayAddSubject) {
            document.addEventListener('click', handleClickOutsideSubject, true);
        }
        return () => {
            document.removeEventListener('click', handleClickOutsideSubject, true);
        };
    }, [displayAddSubject]);

    const addSubject = () => {
        //add only if we don't have it already
        if (!subjects.some(subjectItem => 
          (subjectItem.name === subject.name) && (subjectItem.faculty === subject.faculty) &&
          (subjectItem.level === subject.level) && (subjectItem.year === subject.year)
        )) {
          setSubjects(
              [...subjects, subject]
            );
          }
          //if displaytypesubject is true we for sure have 
          //entered a new subject
          setSubject({
            year:"",
            level:"",
            name:"",
            faculty:"",
            id: ""
          }); // Clear the subject input 
          setdisplayTypeSubject(false);
          setDisplayLevel(false);
          setDisplaySelectSubject(false);
    }
    const removeAddedSubject = (subject) => {
      console.log(subjects)
      console.log(subject)
      setSubjects((prevSubjects)=>prevSubjects.filter((sub) => 
        (sub.name !== subject.name) || (sub.faculty !== subject.faculty)
        || (sub.year !== subject.year) || (sub.level !== subject.level)
      ))
    }
    const { error:subjectRemoveError, data:subjectRemoveData } = useQuery({
      queryKey:["subjects"],
      queryFn: () => {
        //false, we are getting the users' subjects, not others
        return makeRequest.get("/subject/false").then((res) => {
          return res.data;
        })
      }
    });
    const subjectRemoveMutation = useMutation({
      mutationFn: (subjectIds) => {
        // i dont know why i had to specify "data" there otherwise it didn't work error 400 bad req
        return makeRequest.delete("/subject", { data: subjectIds });
      },
      onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries(["subjects"]);
          setSelectedRemoveSubjects([]);
      },
    });
    const handleRemoveSubjectElement = (subjectId) => {
      setSelectedRemoveSubjects((prevSelectedSubjects) =>
        prevSelectedSubjects.includes(subjectId)
        //if the id is in the array already we remove this item
          ? prevSelectedSubjects.filter((id) => id !== subjectId)
          //else we put this item in the array
          : [...prevSelectedSubjects, subjectId]
      );
    }
    const handleRemoveSubjects = () => {
      subjectRemoveMutation.mutate({ids:selectedRemoveSubjects});
      //notifikacija mozda
      //setNotif(true);
    }
    /*useEffect(() => {
      setSubject((prev)=>({...prev,new:displayTypeSubject}))
    }, [displayTypeSubject]);*/
    useEffect(() => {
      // if there are no selected subjects we remove the remove checkmark button
      selectedRemoveSubjects.length > 0 ?
      setDisplayRemoveSubjectButton(true):
      setDisplayRemoveSubjectButton(false);
    }, [selectedRemoveSubjects]);
    //console.log(displaySelectSubject)
    //console.log(subjects)
    //console.log(subject)
    //console.log(subjectData)
    return (
      <div className="updateSubject">
        <div className="subjectNav">  
          <div className="addSubjectContainer">
              <span onClick={()=>{setDisplayAddSubject(true);setDisplayRemoveSubject(false)}} 
              className="addSubjectButton">Add Subject</span>
          </div>
          <div className="removeSubjectContainer">
              <span onClick={()=>{setDisplayRemoveSubject(true);setDisplayAddSubject(false)}} 
               className="removeSubjectButton">Remove Subject</span>
          </div>
        </div>
        {displayAddSubject && <div ref={subjectRef}
         style={{display:"flex", flexDirection:"column", alignItems:"stretch", border:"2px solid white", padding:"5px "}}>
          <label style={{display:"inline"}} htmlFor="selectSubject">Choose a faculty:
            <span style={{fontSize:"small", display:"inline"}}> (which field of study is your subject)</span>
          </label>
          <div style={{display:"flex",flexDirection:"column"}}>
            <div className="selectionsContainer" style={{display:"flex",flexDirection:"row",gap:"10px", flexWrap:"wrap"}}>  
              <select style={{ padding:"3px",color:"black",backgroundColor:"white"}} id="selectSubject"
               name="faculty" onChange={(e)=>{
                  e.target.value === ""?setDisplayLevel(false):setDisplayLevel(true);
                  handleChange(e);
                  }} value={subject.faculty}>
                  <option value="" disabled>Select a faculty</option>
                  <option value="FHŠ">UP FHŠ</option>
                  <option value="FM">UP FM</option>
                  <option value="FAMNIT">UP FAMNIT</option>
                  <option value="PEF">UP PEF</option>
                  <option value="FTŠ TURISTICA">UP FTŠ TURISTICA</option>
                  <option value="FVZ">UP FVZ</option>
              </select>
              {displayLevel && <>
                <select value={subject.level} name = "level" onChange={handleChange}>
                  <option value="" disabled >Select a cycle</option>
                  <option value="1st">1st cycle</option>
                  <option value="2nd">2nd cycle</option>
                  <option value="3rd">3rd cycle</option>
                </select>
                {/*when we selected level above this showed up*/}
                {subject.level === "1st"? <>
                  <select value={subject.year} name="year" onChange={(e)=>{
                    e.target.value === ""? setDisplaySelectSubject(false):setDisplaySelectSubject(true)
                    handleChange(e);
                    }}>
                    <option value="" disabled >Select a year</option>
                    <option value="1">1st year</option>
                    <option value="2">2nd year</option>
                    <option value="3">3rd year</option>
                  </select>
                </>
                : subject.level === "2nd"? <>
                  <select name="year" value={subject.year} onChange={(e)=>{
                    e.target.value === ""? setDisplaySelectSubject(false):setDisplaySelectSubject(true)
                    handleChange(e);
                    }}>
                    <option value="" disabled >Select a year</option>
                    <option value="1">1st year</option>
                    <option value="2">2nd year</option>
                  </select>
                </>
                : <></>
              }
              </>}
              {(displaySelectSubject && subject.level !== "3rd") && <select name="name"value={subject.name}
                onChange={(e)=>{handleChange(e); if(e.target.value==="add"){
                    setSubject((prev)=>({...prev, name:""}));
                    setdisplayTypeSubject(true);return}
                  setdisplayTypeSubject(false);
                  //we placed the id at the end of the name (in value) so we can retreive it here 
                  //and attach it as id in subject, only old ones have an id
                  //new subjects will get an id after insertion
                  setSubject((prev)=>({...prev, id:e.target.value.split(" ")[e.target.value.split(" ").length - 1]}));
                  }
                }>
                  <option value="" disabled >Select a subject</option>
                  {isLoading ? "loading"
                   : subjectError ? subjectError
                   //since these are already existing they have an id so we append it at the end of the value
                   : <>{subjectData.map((sub)=>(<option style={{color:"black"}} value={sub.name +" "+ sub.id} key={sub.id}>
                        {sub.name}
                     </option>))}</>
                  }
                  <option value="add" >Add new +</option>
                </select>
              }
                 {/*no year selection for 3rd cycle phD */}
              {(displayTypeSubject || subject.level === "3rd") && <div className="displayAddSubjectDiv">
                <input type="text" style={{border:"none",background:"transparent"}} name = "name" value = {subject.name}
                 onChange={(e)=>{handleChange(e);
                }} placeholder = "Enter Subject">
                </input>
                {/*{displayConfirmAddSubject && <button onClick={()=>{setDisplayLevel(true)}} type = "button" className="confirmSubjectButton">&#10004;</button>}*/}
              </div>} 
              {subject.name!=="" && isConfirmButton && <>
                {/*if a name exists we can submit the subject 
                and if that name is not already in our list (isConfirmButton handles that)*/}
                <button onClick={addSubject} type = "button" className="confirmSubjectButton">&#43;</button>
              </>}
              <span style={{color:"red"}}>{error && error}</span>
            </div>
            <div className="displaySelectedSubjects">
              {(subjects && subjects.length > 0) && <div className="selectedAddLocations">
                <span style={{color:"white", fontSize:"max(2vw,18px)"}}>New subjects:</span>
                  {subjects.map((subject)=>(
                    <div className="selectedAddLocationsElement" key={
                      subject.name+"&"+subject.faculty+"&"+subject.level+"&"+subject.year
                    }>
                      {/*if we have an id that means it is an old subject so we remove the id we placed at the end
                        otherwise we just display the name because there is nothing after the name */}
                      <span style={{width:"90%"}}>{subject.id?subject.name.split(" ").slice(0, -1).join(" "):subject.name}</span>
                      <button onClick={()=>removeAddedSubject(subject)}
                              style={{border:"none", background:"none", cursor:"pointer"}}>x</button>
                    </div>
                  ))}
              </div>}
            </div>
          </div> 
        </div>}
        {displayRemoveSubject && <div className="removeSubjectDiv">
          {subjectRemoveError ? "Something went wrong"
            : 
            subjectRemoveData?.length > 0 ? subjectRemoveData.map((subject)=>(
              <div key={subject.id}  className="removeLocationElement">
                <input  
                  id = {subject.id}
                  type="checkbox"
                  //state of this check will depend on whether the item is in the selected elements
                  //if it is this will be checked otherwise unchecked
                  checked={selectedRemoveSubjects.includes(subject.id)}
                  //when we click this item the selected elements for removal are updated
                  //if this item's id is not there we add it and if its there we remove it
                  //after which the checked property updates
                  onChange={() => handleRemoveSubjectElement(subject.id)} 
                  />
                  <label htmlFor={subject.id}>{subject.name}</label>
              </div>
              ))
              : "no subjects"
          }
          {displayRemoveSubjectButton && <><button onClick={handleRemoveSubjects} type = "button" className="confirmLocationRemoveButton">&#10004;</button>
              <span style={{fontSize:"small",color:"rgb(90,90,90)"}}>immediately removes subject</span></>}
          </div>
        }
      </div>
    )
  }
  
  export default UpdateSubject