import  { useContext, useState, useEffect,useRef } from 'react';
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { AuthContext } from "../context/AuthContext.js";
import MarkerMap from "./MarkerMap.js"
import "./Contract.css";

const Contract = ({ openContract , authorId , postId, subjectId }) => {
  const { user } = useContext(AuthContext);
  const [ error, setError ] = useState(null);
  const [ success, setSuccess ] = useState(false);
  const [ openSelectLocation, setOpenSelectLocation ] = useState(false);
  const [ locationName, setLocationName ] = useState("")
  const tomorrow = new Date(new Date());
  tomorrow.setDate(tomorrow.getDate() + 1);
  const mapRef = useRef();
  console.log(subjectId)
  const [contractData, setContractData] = useState({
    subject_id:subjectId,
    amount:"",
    date:"",
    timeFrom:"",
    hours:"",
    location_id:"",
    post_id:postId,
    receiver_id:authorId,
    status:"pending"
  });
  const { error:locationError,isLoading, data:locations } = useQuery({
    queryKey:[`locations`,postId,authorId],
    queryFn: () => {
      return makeRequest.get("/locations/"+authorId).then((res) => {
        console.log(res.data);
        return res.data;
      })
    }
  });
  const queryClient = useQueryClient();
  const mutation = useMutation({
    mutationFn: (contract) => {
      return makeRequest.post("/contract/", contract);
    },
    onSuccess: () => {
      queryClient.invalidateQueries(["contracts"]);
    },
  });
  useEffect(() => {
    if (error) {
      const timer = setTimeout(() => {
          setError('');
      }, 2500); 
      return () => clearTimeout(timer);
    }
    if (success) {
      const timer = setTimeout(() => {
          setSuccess(false);
      }, 2500); 
      return () => clearTimeout(timer);
    }
  }, [error,success]);
  const handleClickOutside = (event) => {
    if (mapRef.current && !mapRef.current.contains(event.target)) {
        setOpenSelectLocation(false);
    }
  };
  useEffect(() => {
    if (openSelectLocation) {
        document.addEventListener('click', handleClickOutside, true);
    }
    return () => {
        document.removeEventListener('click', handleClickOutside, true);
    };
  }, [openSelectLocation]);
  const handleChange = (e) =>{
    setContractData((prev) => ({...prev, [e.target.name]:e.target.value}))
  }
  const handleMapClick = (locationId,locationName) =>{
    setContractData((prev)=>({...prev, location_id:locationId}));
    setOpenSelectLocation(false);
    setLocationName(locationName);
  }
  const handleSubmitOffer = () => {
    if(contractData.amount === "" || contractData.date === "" || contractData.timeFrom === ""
     || contractData.hours <= 0 || contractData.hours > 8 
     || contractData.location_id === "") return setError("Please fill in all the fields(correctly)"); 
    mutation.mutate(contractData);
    setContractData({
      subject_id:subjectId,
      amount:"",
      date:"",
      timeFrom:"",
      hours:"",
      location_id:"",
      post_id:postId,
      receiver_id:authorId,
      status:"pending"
    });
    setSuccess(true);
  }
  console.log(contractData);

  return (
    <div className="contract">
      <form style={{width:"min-content"}}>
        {user.role==="tutor"?"Offer your knowledge:"
        :user.role==="student"?"Request knowledge:"
        :"guest"}
        {/*guest functionality is still questionable this is just a placeholder for now */}
        <label htmlFor="timeFrom">Pick a time:</label>
        <input value={contractData.timeFrom} id='timeFrom' name='timeFrom' type="time" step="1800" onChange={(e)=>{handleChange(e)}}></input>
        <label htmlFor="date">Pick a date:</label>
        <input value={contractData.date} id='date' name='date' type="date" onChange={(e)=>{handleChange(e)}} min={tomorrow.toISOString().split('T')[0]}></input>
        <label htmlFor="amount">Amount per hour(in Euros):</label>
        <input value={contractData.amount} id='amount' name='amount' type="number" onChange={(e)=>{handleChange(e)}}></input>€
        <label htmlFor="hours">How many hours:</label>
        <input value={contractData.hours} id='hours' name='hours' type="number" onChange={(e)=>{handleChange(e)}} placeholder="From 1 to 8" min={1}></input>
        {error && <span style={{color:"red"}}>{error}</span>}
        {success && <span style={{color:"green"}}>Contract offered</span>}
        <button type="button" className = "closeMapButton" onClick={()=>setOpenSelectLocation(true)}>Select Location</button>
        <span>Location: {locationName?locationName:"none"}</span>
      </form>
      {openSelectLocation && <MarkerMap handleMapClick = {handleMapClick} mapRef = {mapRef} locations={locations}/>}
      <button type='button' className="submitOfferButton" onClick={handleSubmitOffer}>Offer</button>
      <button className = "closeContractButton" onClick = {()=>{openContract(false)}}>X</button>
    </div>
  )
}
  
export default Contract