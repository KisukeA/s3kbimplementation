import { useContext } from "react";
import { AuthContext } from "../context/AuthContext.js";
import { makeRequest } from "../axios.js"
import { Link } from "react-router-dom";
import { useQuery } from "@tanstack/react-query";
import "./MiniUser.css";

const MiniUser = ({ searchedUser }) => {
  const { error:subjectError, isLoading, data:subjectData } = useQuery({
    queryKey:["subjects",searchedUser],
    //this one with "s" the url is not
    queryFn: () => {
      //true that we are fetching others posts
      return makeRequest.get("/subject/true",{ params:{userId:searchedUser.id} }).then((res) => {
          return res.data;
        })
      }
  });
  const { user } = useContext(AuthContext);
  return (
      <div>
      <Link to={ searchedUser.id === user.id ? `/profile/`+ user.id : `/userprofile/`+ searchedUser.id}
            style={{textDecoration:"none"}} >
        <div  className="searchElementUser">
          <div className="searchElemFirstColumn">
            <img src={`/upload/${searchedUser.profilePicture?searchedUser.profilePicture:"noPP.jpg"}`}
             className = "searchProfilePicture"></img>
            <div className="searchNames">  
              <span>{searchedUser.username}</span>
              <span style={{fontSize:"small", color:"rgb(70, 70, 70)"}}>{searchedUser.nickname}</span>
            </div>
          </div>
          <div className="mappedSubjects">
            {isLoading?"loading":<>Subjects: 
            <div className="searchElemSubjects">
              {subjectData.length > 0 ? subjectData.map((sub)=>(<span key={sub.id}>{sub.name}({sub.rating})</span>))
              : "none"
              }
            </div>
            </>}
          </div>
        </div>
      </Link>
    </div>
  )
}
  
  export default MiniUser