import React, { useEffect } from 'react';
import mapboxgl from 'mapbox-gl';


mapboxgl.accessToken = 'pk.eyJ1Ijoidmluc21va2UiLCJhIjoiY2xzdGtpb2dmMTF1ZjJyczBpdHN1bjN6MCJ9.ctEn8V4u_p-SlP9EJOOwuw';

const MarkerMap = ({ locations, mapRef, handleMapClick }) => {
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: 'map', // container ID
      style: 'mapbox://styles/mapbox/streets-v11',
      center: locations?.length>0?[locations[0].longitude, locations[0].latitude]: [13.7302, 45.5481], //Koper
      zoom: 7 // starting zoom
    });
    const markers = []
    // adding markers to the map
    locations?.forEach(location => {
      console.log(location);
      // Create a DOM element for each marker
      const el = document.createElement('div');
      var zoom = map.getZoom();
      var baseSize = 15;
      var size = baseSize * zoom / 5;
      el.className = 'marker';
      el.style.backgroundImage = 'url(https://cdn-icons-png.flaticon.com/512/1673/1673221.png)'; // Example marker image
      el.style.width = `${size}px`;
      el.style.height = `${size}px`;
      el.style.backgroundSize = '100%';

      var popup = new mapboxgl.Popup({ offset: 25 }).setHTML(`<div>
      <h3>${location.placeName}</h3>
      <span id="clickable-span-`+location.placeName+`" style="cursor:pointer; outline:1px solid black; text-decoration: none;">
      Select</span>
      </div>`);
      popup.on('open', () => {
        const clickableSpan = document.getElementById(`clickable-span-${location.placeName}`);
        if (clickableSpan) {
          clickableSpan.onclick = () => {
            handleMapClick(location.id,location.placeName);
            map.fire('closePopup');
          };
        }
      });
      // Add a marker to the map
      new mapboxgl.Marker(el)
        .setLngLat([location.longitude, location.latitude])
        .setPopup(popup)
        .addTo(map);
        markers.push(el);
      map.on('closePopup', () => {
        popup.remove();
      });
      });
      map.on('zoom', () => {
        var zoom = map.getZoom();
        markers.forEach((el) => {
          var baseSize = 15;
          var size = baseSize * zoom / 5;
          el.style.width = `${size}px`;
          el.style.height = `${size}px`;
        });
      });
      
  }, [locations]);
  return (<><div id="map" style={{ width: '400px', height: '400px' }} ref={mapRef} />
</>)
};

export default MarkerMap;