import moment from "moment";
import { Link } from "react-router-dom";
import ContractView from "./ContractView.js";
import { AuthContext } from "../context/AuthContext.js";
import { useContext, useState, useEffect } from "react";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { makeRequest } from "../axios.js" ;
import "./Notification.css";

const Notification = ({contract}) => {
    const { user } = useContext(AuthContext);
    //maybe add a timeUpdated column in the database so we sort by the newest one
    const cdate = new Date(contract.date)
    const year = cdate.getFullYear();
    const month = (cdate.getMonth() + 1).toString().padStart(2, '0');
    const day = cdate.getDate().toString().padStart(2, '0');
    const date = `${year}-${month}-${day}`

    const queryClient = useQueryClient();
    const deleteMutation = useMutation({
        mutationFn: (contractId) => {
          return makeRequest.delete("/contract/"+contractId);
        },
        onSuccess: () => {
          queryClient.invalidateQueries(["notiContracts"]);
          
        },
    });
    const mutation = useMutation({
        mutationFn: (contract) => {
          return makeRequest.put("/contract/", contract);
        },
        onSuccess: () => {
          //queryClient.invalidateQueries(["myContracts","contracts"]);
          queryClient.invalidateQueries(["myContracts","notiContracts"]);
        },
    });
    const handleDelete = () => {
        deleteMutation.mutate(contract.id);
    }
    //maybe create a notification table for this
    const handleDismiss = () => {
        mutation.mutate({
          status:"seen",
          id:contract.id,
          counter:false
        })
        //since status is not declined/accepted or cancelled anymore 
        //it wont come again as a notification
        //and because it is seen it will go to the fetched MyContracts data
        //unless, of course, it is an old unopened notification and the time has passed,
        //then it will go to the passed, not upcoming
    }
    const handleCancel = (approved) =>{
      //we set the status accordingly and now this becomes a notification to the other user
      approved?mutation.mutate({
        status:"approved",
        id:contract.id,
        counter:false
      }):mutation.mutate({
        //this is a problem, (maybe), we should go to status seen again but then other user
        //wouldnt get the notification, like this we will only go there once the user sees the noti
        //but its not that big of a deal, the other user getting the noti is more important
        status:"refused",
        id:contract.id,
        counter:false
      })
    }
    const handleCancelAnswer = (response) =>{
      //if the cancel response was approved we delete the contract
        return response?deleteMutation.mutate(contract.id):mutation.mutate({
          status:"seen",
          id:contract.id,
          counter:false
        })
        //otherwise we revert it back to what it was, but to "seen" not "accepted",
        //because this is an old contract we dont need it to appear in a notification again
    }
    return (
      <div className="notification">
        <span>{moment(date.split('T')[0]+'T'+contract.timeFrom).fromNow()}</span>
        <span>({new Date(date).toLocaleDateString('en-GB')})</span>
        <span className="twoCol">Contract between: </span>
        <span className="aHoverContainer twoCol"> -
          <Link to = {`/userprofile/`+contract.offerer_id} className="aViewProfile"> {contract.username} 
          </Link>
        </span>
        <span className="aHoverContainer twoCol"> -
          <Link to = {`/userprofile/`+contract.receiver_id} className="aViewProfile"> {contract.receiver_username} 
          </Link>
        </span>
        <span>{contract.amount}€ per hour</span>
        {contract.status === "declined" && <>
            <span className = "twoCol">Offer declined</span>
            <button onClick={handleDelete} className = "viewContractButton twoCol">Delete</button>
        </>}
        {contract.status === "accepted" && <>
            <span className = "twoCol">Offer accepted</span>
            <button onClick={handleDismiss} className = "viewContractButton twoCol">Dismiss</button>
        </>}                         {/*this user is not the one who requested the cancellation */}
        {contract.status === "cancelled" && user.id!==contract.canceller_id && <>
            <span className="twoCol"><span>{contract.canceller_id===contract.offerer_id?contract.username:contract.receiver_username} </span>
            requested cancel</span>
            <button onClick={()=>{handleCancel(true)}} className = "viewContractButton oneCol">Approve</button>
            <button onClick={()=>{handleCancel(false)}} className = "viewContractButton oneCol">Refuse</button>
        </>}    {/*if we are the one who requested the cancel it will stay as a notification*/}
        {contract.status === "cancelled" && user.id===contract.canceller_id && <>
            <span className="twoCol">You requested cancel</span>
        </>} 
                          {/*now we display the notification only to the one who requested cancellation */}
        {contract.status === "approved" && user.id===contract.canceller_id && <>
            <span className="twoCol">Cancel approved</span>
            <button onClick={()=>{handleCancelAnswer(true)}} className = "viewContractButton twoCol">I understand</button>
        </>}                            {/*same here*/}
        {contract.status === "refused" && user.id===contract.canceller_id && <>
            <span className="twoCol">Cancel refused</span>
            <button onClick={()=>{handleCancelAnswer(false)}} className = "viewContractButton twoCol">I understand</button>
        </>}
        {contract.status === "approved" && user.id!==contract.canceller_id && <>
            <span className="twoCol">you approved to cancel</span>
        </>}                            {/*same here*/}
        {contract.status === "refused" && user.id!==contract.canceller_id && <>
            <span className="twoCol">you refused to cancel</span>
        </>}
      </div>
    )
  }
  
  export default Notification