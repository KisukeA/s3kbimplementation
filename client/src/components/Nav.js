import { useNavigate, Link } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext.js";
import { makeRequest } from "../axios.js";
import Search from "./Search.js";
import "./Nav.css";

const Nav = () => {
  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();
  const logout = async () => {
    const r = await makeRequest.post("/auth/logout",{},{withCredentials:true});
    setUser(null);
    navigate("/login");
    //return <Navigate to="/landing" />;
  }
    return (
      <div className="navBar">
        <Link className="aNav" to = "/">
          <img className = "aNavLogo" src={`/upload/Kagurabachi_Logo.webp`}/>
          <span style={{color:"black", fontWeight:"700", fontFamily:"Helvetica"}}>Kagura Bachi</span>
        </Link>
        <Search/>
        <button onClick = {logout} className="logoutbtn">Log out</button>
      </div>
    )
  }
  
  export default Nav