import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContext.js";
import { useContext, useState, useEffect } from "react";
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import MiniContract from "./MiniContract.js";
import "./LeftBar.css";

const LeftBar = () => {
    const { user } = useContext(AuthContext);
    const [ unansweredOpen, setUnansweredOpen ] = useState(false);
    const [ counterOpen, setCounterOpen ] = useState(false);
    const [ pendingOpen, setPendingOpen ] = useState(false);
    const [ notClickedC, setNotClickedC ] = useState(true);
    const [ notClickedI, setNotClickedI ] = useState(true);
    const [ notClickedP, setNotClickedP ] = useState(true);
    const { isLoading,error, data } = useQuery({
      queryKey:["contracts"],
      queryFn: () => {
          return makeRequest.get("/contract/").then((res) => {
            setNotClickedI(true);
            return res.data;
          });
      }
    });
    const { isLoading:cIsLoading,error:cError, data:cData } = useQuery({
      queryKey:["counters"],
      queryFn: () => {
          return makeRequest.get("/contract/counter").then((res) => {
            setNotClickedC(true);
            //notification semi working xd both of them light up when a new offer comes up be it counter or a normal one
            return res.data;
          });
      }
    });
    const { isLoading:pIsLoading,error:pError, data:pData } = useQuery({
      queryKey:["pending"],
      queryFn: () => {
          return makeRequest.get("/contract/pending").then((res) => {
            setNotClickedP(true);
            return res.data;
          });
      }
    });
    return (
      <div className="leftBar">
        <Link to = {`/profile/${user.id}`} className="aProfile">
          <img style = {{margin:"0"}} src={`/upload/${user.profilePicture?user.profilePicture:"noPP.jpg"}`} 
            className="userProfilePicture" />
        </Link>
        {/*close the other 2 and remove the notification */}
        <button className="aViewContracts" onClick={()=>{setUnansweredOpen(!unansweredOpen);setNotClickedI(false); setCounterOpen(false);setPendingOpen(false)}}>
          Incoming {(data?.length&& notClickedI)?<span className="leftBarNotiSpan">{data?.length}</span>:<></>}</button>
        <button className="aViewContracts" onClick={()=>{setCounterOpen(!counterOpen);setNotClickedC(false); setPendingOpen(false);setUnansweredOpen(false)}}>
          Counter {(cData?.length && notClickedC)?<span className="leftBarNotiSpan">{cData?.length}</span>:<></>}</button>
        <button className="aViewContracts" onClick={()=>{setPendingOpen(!pendingOpen); setCounterOpen(false);setUnansweredOpen(false)}}>
          Pending </button>
        <div className="contractsContainer">
          {unansweredOpen && <> {isLoading?"loading"
          :<>
            <span>Incoming Contracts:</span> {data.length>0?data?.map((contract)=>(<MiniContract key = {contract.id} contract={contract} />))
            :<>none</>
            }
           </>
          }</>}
          {counterOpen && <>{cIsLoading?"loading"
          :<>
            <span>Counter offers:</span> {cData.length>0?cData?.map((contract)=>(<MiniContract key = {contract.id} contract={contract} />))
            :<>none</>
            }
           </>
          }</>}
          {pendingOpen && <>{pIsLoading?"loading"
          :<>
            <span>Pending offers:</span> {pData.length>0?pData?.map((contract)=>(<MiniContract key = {contract.id} pending={true} contract={contract} />))
            :<>none</>
            }
           </>
          }</>}
        </div>
      </div>
    )
  }
  
  export default LeftBar