import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContext.js";
import { useContext, useState, useEffect } from "react";
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import MiniContract from "./MiniContract.js";
import moment from "moment-timezone";
import "./Timetable.css";

const Timetable = ({user, timetableRef}) => {
  //variables to manage state of animation
  const [activeAnimation, setActiveAnimation] = useState(false);
  const [activeAnimationB, setActiveAnimationB] = useState(false);
  let originalStartOfWeek;
  let originalEndOfWeek;
  const todaysDay = new Date().getDay() || 7 ;//we want 7 for sunday not 0
  const [currentTime, setCurrentTime] = useState(moment().tz("Europe/Ljubljana").format('HH:mm'));
  useEffect(() => {
    const interval = setInterval(() => setCurrentTime(moment().tz("Europe/Ljubljana").format('HH:mm')), 60000);
    return () => {
      clearInterval(interval);
    };
  }, []);
  const getWeekStartAndEnd = (date = new Date()) =>{
    const dayOfWeek = date.getDay();
    //todays day
    const start = new Date(date);
    //to find monday we subtract todays date with the todays day but add 1 since monday is indexed 1 in js
    //if it is sunday (index 0) we don't add 1 since that's a new week but rather we subtract 6 days
    start.setDate(date.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1)); // Adjust to Monday
    start.setHours(0, 0, 0, 0); // Set to start of the day

    const end = new Date(start);
    //end of week is add 6 days to monday
    end.setDate(start.getDate() + 6); // Move to end of the week (Sunday)
    end.setHours(23, 59, 59, 999); // Set to end of the day
    //setStartOfWeek(start);
    //setEndOfWeek(end);
    originalStartOfWeek = start;
    originalEndOfWeek = end;
    return {start:start, end:end}
  }
  const [ startOfWeek, setStartOfWeek ] = useState(getWeekStartAndEnd().start);
  const [ endOfWeek, setEndOfWeek ] = useState(getWeekStartAndEnd().end);

  const { isLoading,error, data } = useQuery({
    queryKey:["timetableContracts", user.id],
    queryFn: () => {
        return makeRequest.get("/contract/user",{ params:{userId:user.id} }).then((res) => {
          console.log(res.data)
          return res.data;
        });
    }
  });
  const groupByDayOfWeek = (data) => {
    const groups = Array.from({ length: 7 }, () => []); // create 7 arrays for each day of the week

    data?.forEach(item => {
      //for each contract we get the date and check if its in the current week
        const itemDate = new Date(item.date);
        if (itemDate >= startOfWeek && itemDate <= endOfWeek) {
          // if it is in the current week we add it to its corresponding array
          //(we get monday 1 to sunday 0 index, so instead of 0 we want 7 and subtract 1 ↓)
            const dayIndex = (itemDate.getDay() || 7) - 1; // subtract 1 so Monday is the 0 index Sunday is 6
            groups[dayIndex].push(item);
        }
    });
  return groups;
  }
  const weekData = groupByDayOfWeek(data);
  const daysOfWeek = ["Monday", "Tuesday", "Wednesday","Thursday","Friday","Saturday","Sunday"];
  const hoursOfDay = Array.from({ length: 15 }, (_, i) => 7 + i); // from 7 to 21 hr
  function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  const changeWeek = async (next) => {
    const nextStart = new Date(startOfWeek);
    const nextEnd = new Date(endOfWeek);
    if (next) {
      setActiveAnimation(true);
      //animate the one going left
      await delay(100);
      nextStart.setDate(nextStart.getDate() + 7);
      nextEnd.setDate(nextEnd.getDate() + 7);
    }
    else{
      setActiveAnimationB(true);
      //animate the one going right
      await delay(100);
      nextStart.setDate(nextStart.getDate() - 7);
      nextEnd.setDate(nextEnd.getDate() - 7);
    }
    setStartOfWeek(nextStart);
    setEndOfWeek(nextEnd);
  }
  useEffect(() => {
    // remove the class name that makes it animate after half a sec
    if (activeAnimation) {
        const timer = setTimeout(() => {
            setActiveAnimation(false); 
            //document.getElementById("after").style.left = "1rem";
        }, 500); 

        return () => clearTimeout(timer);
    }
}, [activeAnimation]);
useEffect(() => {
  // remove the class name that makes it animate after half a sec
  if (activeAnimationB) {
      const timer = setTimeout(() => {
          setActiveAnimationB(false); 
          //document.getElementById("after").style.left = "1rem";
      }, 500); 

      return () => clearTimeout(timer);
  }
}, [activeAnimationB]);
  console.log();
  return (
    <div className="timetableContainer" ref={timetableRef}>
        <div className ="timetableDiv">
        {/*this is for my testing animation, sliding from one week to another */}
        <span className={`before ${activeAnimationB?'animateB':''}`}></span>
          <span className="displayWeek start">{startOfWeek.toDateString()}</span>
          <div className="header-row">
            <span className="previousWeek" onClick={()=>{changeWeek(false)}}></span>
            <div className="empty-cell"></div>
            {/*first we fill out the headers */}
            {daysOfWeek.map((day, index)=> (
              //+1 because monday is 1 in dates and sunday is 7 but indexes are 0 to 6
              <div key={day} className="header-cell">{day}</div>
            ))}
            <span className="nextWeek" onClick={()=>{changeWeek(true)}}></span>
          </div>
          <span className="displayWeek end">{endOfWeek.toDateString()}</span>
          {/*now we go row by row */}
          {hoursOfDay.map(hour => (
            <div key={hour} className="row">
              <div className="hours">{hour.toString().padStart(2, '0')}</div>
              {/*we fill horizontally, day after day */}
              {daysOfWeek.map((day, index) => {
                {/*if the hour we are at is between the start and end time of the contract it becomes a cell */}
                const event = weekData[index]?.find(event => (
                  parseInt(event.timeFrom.split(':')[0]) === hour))
                return (
                  //we subtract 5 because the hour starts from 7, but rowstart/end starts from 1, 
                  //but 7-5 = 2 because we start from the 2nd row, 1st row is just the headers
                  <span key={day + hour} className={`cell${(index+1===todaysDay 
                    /*&& ((originalStartOfWeek <= eventDate && originalEndOfWeek >= eventDate))*/)?` glow`:``}`}>
                    {/*we also have to check if its the current week since we can display more weeks now */}
                    {/*if the cell doesn't have an event(hence, no event.date), we disiplay the background glow*/}
                    {/*NOT WORKING!!!!!! have to think of something better */}
                    {event ?                                                                                  //if its 0 NaN we just add 1 and subtract 1 so we end up doint nothing
                      <div className={`event`} style={{'--row-start':`${(hour - 5)}`,
                      '--row-end':`${(hour - 5) + (parseInt(event?.timeTo.split(':')[0]) || 1) - (parseInt(event?.timeFrom.split(':')[0]) || 1)}`,
                      '--minutes-offset':`${parseInt(event?.timeFrom.split(":")[1])}`}}>
                        {event.name}
                      {/* style = {{gridRowStart:`${(hour - 5)}`, gridRowEnd:`${(hour - 5) + (parseInt(event?.timeTo.split(':')[0]) || 1) - (parseInt(event?.timeFrom.split(':')[0]) || 1)}`}}*/}
                        <span className = "eventContent">{new Date(moment(event).tz("Europe/Ljubljana")).toDateString().split(" ").slice(1,).join(" ")}<br></br> 
                         <span>{`from: ` + event.timeFrom}</span>
                         <span>{`to: ` + event.timeTo}</span>
                         <span>{event.username},</span>
                         <span>{event.receiver_username}</span>
                         <span>{event.locationName}</span>
                         </span>
                      </div>
                    : ''}
                    {hour === parseInt(currentTime.split(":")[0]) && todaysDay === index+1 &&
                     <span className="currentTimeIndicator" style={{'--minutes-offset':`${parseInt(currentTime.split(":")[1])}`}}></span>
                    }
                  </span>
                );
              })}
            </div>
          ))}
          <span className={`after ${activeAnimation?'animate':''}`}></span>
        </div>  
    </div>
  )
}
  
  export default Timetable
  /*<div className="timetableDiv">
          {weekData.map((slots, index)=>(
          <div>
            <div key={index} className="header-cell">{daysOfWeek[index]}</div>
              {slots.map((slot)=>(
                <div key={slot.id} className="cell">
                  {slot.timeTo.split(":")[0] - slot.timeFrom.split(":")[0]}
                </div>
              ))}
          </div>
          ))}
        </div>*/