import { useContext } from "react";
import { AuthContext } from "../context/AuthContext.js";
import { Link } from "react-router-dom";
import "./MiniPost.css";
import moment from "moment"

const MiniPost = ({ post }) => {
    const { user } = useContext(AuthContext);
    return (
        <div>
        {/*we send the post id and the author id to retreive the necessary info for the post */}
        <Link to={`/singlepost/`+post.id+`/`+post.author_id} style={{textDecoration:"none"}} >
          <div className="searchElement">
            {/*<img src={`/upload/${searchedUser.profilePicture?searchedUser.profilePicture:"noPP.jpg"}`}
             className = "searchProfilePicture"></img>*/}
            <div className="searchNames">  
              <div className="searchElemFirstRow">  
                <span>{post.type}</span>
                <span>{post.subjectName?`Subject: `+post.subjectName:''}</span>
                <span style={{marginLeft:"auto",fontSize:"small", color:"rgb(70, 70, 70)"}}>{moment(post.created).fromNow()}</span>
              </div>
              <span className="miniPostContent">{post.content}</span>
              <span style={{width:"fit-content"}}>{post.keywords}</span>
            </div>
          </div>
        </Link>
      </div>
    )
  }
  
  export default MiniPost