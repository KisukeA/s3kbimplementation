
import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContext.js";
import { useContext, useState, useEffect, useRef } from "react";
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import MiniContract from "./MiniContract.js";
import Timetable from "./Timetable.js";
import momentz from "moment-timezone";
import Notification from "./Notification.js";
import "./RightBar.css";

const RightBar = () => {
    const { user } = useContext(AuthContext);
    const [ timetableOpen, setTimetableOpen ] = useState(false);
    const [ openTimetable, setOpenTimetable ] = useState(false);
    const [ contractsOpen, setContractsOpen ] = useState(false);
    const [ notificationsOpen, setNotificationsOpen ] = useState(false);
    const [ activeButton, setActiveButton ] = useState(null);

    const timetableRef = useRef();
    const { isLoading,error, data } = useQuery({
      queryKey:["myContracts", user.id],
      queryFn: () => {
        //these are only "accepted" or "upcoming" contracts
        //which are also accepted but the accept
        return makeRequest.get("/contract/my").then((res) => {
          return res.data;
        });
      }
      //maybe i should add cancelled contracts too, which have not been yet answered
    });
    const { isLoading:nIsLoading,error:nError, data:nData } = useQuery({
      queryKey:["notiContracts", user.id],
      queryFn: () => {
        //these are only "accepted" contracts
          return makeRequest.get("/contract/noti").then((res) => {
            return res.data;
          });
      }
    });
    //so we avoid errors because of status,
    //these are for another type of notification
    //one type are for when user is offerer another when user is receiver
    const { isLoading:n2IsLoading,error:n2Error, data:n2Data } = useQuery({
      queryKey:["notiContracts2", user.id],
      queryFn: () => {
        //these are only "accepted" contracts
          return makeRequest.get("/contract/noti2").then((res) => {
            return res.data;
          });
      }
    });
    const handleClick = (buttonId) => {
      setActiveButton(activeButton === buttonId ? null : buttonId);
    };
    const handleClickOutside = (event) => {
      if (timetableRef.current && !timetableRef.current.contains(event.target)) {
          setOpenTimetable(false);
      }
    };
    useEffect(() => {
      if (openTimetable) {
          document.addEventListener('mousedown', handleClickOutside, true);
      }
    });
    return (
      <div className="rightBar">
        <button className="rightBarButton" onClick = {()=>{setContractsOpen(!contractsOpen);setTimetableOpen(false);setNotificationsOpen(false);setActiveButton(null)}}>Your Contracts</button>
        <button className="rightBarButton" onClick = {()=>{setTimetableOpen(!timetableOpen);setContractsOpen(false);setNotificationsOpen(false)}}>Timetable</button>
        <button className="rightBarButton" onClick = {()=>{setNotificationsOpen(!notificationsOpen);setContractsOpen(false);setTimetableOpen(false)}}> Notifications</button>
        {contractsOpen &&
          <div style={{overflowY:'hidden'}}>
            <button id="upcoming" onClick = {(e)=>{handleClick(e.target.id)}}
            className={`rightBarButton ${activeButton === "upcoming" ? 'activeButton' : ''}`}>
              Upcoming</button>
            <button id="passed"onClick = {(e)=>{handleClick(e.target.id)}}
             className={`rightBarButton ${activeButton === "passed" ? 'activeButton' : ''}`}>
              Passed</button>
            <div className="contractsContainer">
              {activeButton === "upcoming" && data?.filter((con)=>{
                //again here we convert to the actual date before we check if it has passed or not (like in minicontract)
                const cdate = new Date(con.date)
                const year = cdate.getFullYear();
                const month = (cdate.getMonth() + 1).toString().padStart(2, '0');
                const day = cdate.getDate().toString().padStart(2, '0');
                const date = `${year}-${month}-${day}`
                //check if the starting date and time of the contract,(converted to ljubljana time) 
                //is after the current time, which means it is yet to come
                const dateTimeInZone = momentz.tz(`${date}T${con.timeFrom}`,'Europe/Ljubljana');
                return dateTimeInZone.isAfter(momentz.tz('Europe/Ljubljana'));
                }).map((contract)=>(<MiniContract key = {contract.id} upcoming={true} contract={contract} />))}
              {activeButton === "passed" && data?.filter((con)=>{
                //again here we convert to the actual date before we check if it has passed or not
                const cdate = new Date(con.date)
                const year = cdate.getFullYear();
                const month = (cdate.getMonth() + 1).toString().padStart(2, '0');
                const day = cdate.getDate().toString().padStart(2, '0');
                const date = `${year}-${month}-${day}`
                //check if the ending date and time of the contract,(converted to ljubljana time) 
                //is before current time, which means it has passed
                const dateTimeInZone = momentz.tz(`${date}T${con.timeTo}`,'Europe/Ljubljana');
                return dateTimeInZone.isBefore(momentz.tz('Europe/Ljubljana'));
                }).map((contract)=>(<MiniContract key = {contract.id} passed = {true} contract={contract} />))}
            </div>
          </div>
        }
        {timetableOpen &&  
          <div className="timetableButtons">
            <button className="rightBarButton" onClick={()=>setOpenTimetable(!openTimetable)}>open timetable</button>
            <button className="rightBarButton">export timetable</button>
          </div>
        }
        {openTimetable && <Timetable user={user} timetableRef={timetableRef} />}
        {notificationsOpen && <>
          <div className="notificationsContainer" style={{overflowY:"auto"}}>
            {nData?.map((con)=>(<Notification key={con.id} contract={con} />))}
            {n2Data?.map((con)=>(<Notification key={con.id} contract={con} />))}
          </div>
        </>}
      </div>
    )
  }
  
  export default RightBar