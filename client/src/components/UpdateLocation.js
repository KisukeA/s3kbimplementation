import { useState, useEffect, useRef } from "react";
import { makeRequest } from "../axios.js";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import Map from "./Map.js";
import "./UpdateLocation.css";

const UpdateLocation = ({locations, setLocations}) => {

    const [ openLocation, setOpenLocation ] = useState(false);
    const [ displayLocation, setDisplayLocation ] = useState(false);
    const [ displayRemoveLocation, setDisplayRemoveLocation ] = useState(false);
    const [ removeLocation, setRemoveLocation ] = useState(false);
    const [ selectedRemoveLocations, setSelectedRemoveLocations ] = useState([]);
    //const [ locations, setLocations ] = useState([]);
    const [ location, setLocation ] = useState({
      longitude:null,
      latitude:null,
      placeName:null
    });
    
    const locationRef = useRef();
    const removeLocationRef = useRef();
    const queryClient = useQueryClient();
    const { error:locationError, data } = useQuery({
      queryKey:["locations"],
      queryFn: () => {
        return makeRequest.get("/locations").then((res) => {
          return res.data;
        })
      }
    });

    const locationRemoveMutation = useMutation({
      mutationFn: (locationIds) => {
        // i dont know why i had to specify "data" there otherwise it didn't work error 400 bad req
        return makeRequest.delete("/locations/", { data: locationIds });
      },
      onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries(["locations"]);
          setSelectedRemoveLocations([]);
      },
    });
    
    const handleClickOutside = (event) => {
        if (locationRef.current && !locationRef.current.contains(event.target)) {
            setOpenLocation(false);
        }
    };
    const handleClickOutsideRemove = (event) => {
        if (removeLocationRef.current && !removeLocationRef.current.contains(event.target)) {
            setRemoveLocation(false);
            setDisplayRemoveLocation(false);
        }
    };
    useEffect(() => {
        console.log('Location changed:', location);
        setOpenLocation(false);
        if(location.latitude !== null && location.longitude !== null && location.placeName !== null){
          setDisplayLocation(true);
        }
    }, [location]);
    useEffect(() => {
      if (openLocation) {
          document.addEventListener('click', handleClickOutside, true);
      }
      return () => {
          document.removeEventListener('click', handleClickOutside, true);
      };
    }, [openLocation]);
    
    useEffect(() => {
      if (removeLocation) {
          document.addEventListener('click', handleClickOutsideRemove, true);
      }
      return () => {
          document.removeEventListener('click', handleClickOutsideRemove, true);
      };
    }, [removeLocation]);
    const addLocation = () => {
      //at least 1 location has the same placeName as our current selected location
      // if true means that we already have that so not adding
      if (!locations.some(locationItem => locationItem.placeName === location.placeName)) {
        setLocations(
            [...locations, location]
          );
        }
        setLocation({
          longitude:null,
          latitude:null,
          placeName:null
        }); // Clear the location input 
        setDisplayLocation(false);
    };
    const handleRemoveLocationElement = (locationId) => {
      //todo
      setSelectedRemoveLocations((prevSelectedLocations) =>
        prevSelectedLocations.includes(locationId)
        //if the id is in the array already we remove this item
          ? prevSelectedLocations.filter((id) => id !== locationId)
          //else we put this item in the array
          : [...prevSelectedLocations, locationId]
      );
    }
    const handleRemoveLocation = () => {
      locationRemoveMutation.mutate({ids:selectedRemoveLocations});
      //notifikacija mozda
      //setNotif(true);
    }
    useEffect(() => {
      // if there are no selected locations we remove the remove checkmark button (nothing to remove) xd
      selectedRemoveLocations.length > 0 ?
      setDisplayRemoveLocation(true):
      setDisplayRemoveLocation(false);
    }, [selectedRemoveLocations]);
    const removeAddedLocation = (locationPlaceName) => {
      console.log(locations)
      console.log(locationPlaceName)
      setLocations((prevLocations)=>prevLocations.filter((place) => place.placeName !== locationPlaceName))
    }
    return (
      <div className="UpdateLocation">
        <div className="addLocationContainer">
            <span onClick={()=>{setOpenLocation(true);
            setDisplayLocation(false); setDisplayRemoveLocation(false); setRemoveLocation(false);}} 
            style={{width:"fit-content"}} className="addLocationButton">Add Location</span>
            {displayLocation && <button onClick={addLocation} type = "button" className="confirmLocationButton">&#10004;</button>}
          </div>  
          <div ref={removeLocationRef} className="removeLocationContainer">
            <div style = {{display:"flex",flexDirection:"row", gap:"5px 10px", width:"fit-content", alignItems:"center"}}>  
              <span onClick={()=>{setDisplayLocation(false);setOpenLocation(false);setRemoveLocation(true);
                if(selectedRemoveLocations.length > 0){setDisplayRemoveLocation(true)};
                //if user already selected something but clicked off
                //we still keep his selection and just show the confirm button again
                //because it was hidden when user clicked off
              }}
              className="removeLocationButton">Remove Location</span>
              {displayRemoveLocation && <><button onClick={handleRemoveLocation} type = "button" className="confirmLocationRemoveButton">&#10004;</button>
              <span style={{fontSize:"small",color:"rgb(90,90,90)"}}>immediately removes location</span></>}
            </div>
            {removeLocation && <div className="removeLocationsList"> 
              {locationError ? "Something went wrong"
              : 
              data?.length > 0 ? data.map((location)=>(
                <div key={location.id}  className="removeLocationElement">
                  <input  
                    id = {location.id}
                    type="checkbox"
                    checked={selectedRemoveLocations.includes(location.id)}
                    //if item is selected this will be checked otherwise unchecked
                    onChange={() => handleRemoveLocationElement(location.id)} 
                    //when user clicks we handle change here
                    />
                    <label htmlFor={location.id}>{location.placeName}</label>
                </div>
                ))
                : "no locations"
              }
              </div>
            }
          </div>  
          {displayLocation && <span className="displayLocationsSpan">Your location is: {location.placeName}</span>}
          {openLocation && <Map locationRef={locationRef} setLocation={setLocation} />}
          {(locations && locations.length > 0) && <div className="selectedAddLocations">
            <span style={{color:"white", fontSize:"max(2vw,18px)"}}>New locations:</span>
            {locations.map((location)=>(
              <div className="selectedAddLocationsElement" key={location.placeName}>
                <span style={{width:"90%"}}>{location.placeName}</span>
                <button onClick={()=>removeAddedLocation(location.placeName)}
                        style={{border:"none", background:"none", cursor:"pointer"}}>x</button>
              </div>
          ))}</div>}
      </div>
    )
  }
  
  export default UpdateLocation