import { useState, useEffect, useRef } from "react";
import { makeRequest } from "../axios.js";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import UpdateLocation from "./UpdateLocation.js";
import UpdateSubject from "./UpdateSubject.js";
import "./Update.css";

const Update = ({setUpdateOpen, user}) => {
    const [ updateUser, setUpdateUser ] = useState(false);
    const [ err, setErr ] = useState(null);
    const [ profilePic, setProfilePic ] = useState(null);
    const [ locations, setLocations ] = useState([]);
    const [ updateLocation, setUpdateLocation ] = useState(false);
    const [ subjects, setSubjects ] = useState([]);
    const [ updateSubject, setUpdateSubject ] = useState(false);
    const contentRef = useRef();
    const buttonRef = useRef();
    const [ userData, setUserData ] = useState({
        nickname:"",
    });
    const handleClickOutside = (event) => {
      if ((contentRef.current || buttonRef.current) 
          //if we click in the content or the button we dont close the div
          && (!contentRef.current.contains(event.target) && !buttonRef.current.contains(event.target))) {
          setUpdateLocation(false);setUpdateSubject(false);setUpdateUser(false);
          //if we click out of content we close whatever is open
      }
    };
    useEffect(() => {
      if (updateLocation || updateSubject || updateUser) {
        //if any of these are changed we listen for clicks
          document.addEventListener('click', handleClickOutside, true);
      }
      return () => {
          document.removeEventListener('click', handleClickOutside, true);
      };
    }, [updateLocation,updateSubject,updateUser]);
    const upload = async (e) => {
      e.preventDefault();
      try {
        const formData = new FormData();
        formData.append("profile", profilePic);
        const res = await makeRequest.post("/upload", formData);
        return res.data;
      } catch (err) {
        console.log(err);
      }
    };
    const queryClient = useQueryClient();
    const mutation = useMutation({
      mutationFn: (user) => {
        return makeRequest.put("/users", user);
      },
      onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries(["user"]);
          
      },
    });
    const locationAddMutation = useMutation({
      mutationFn: (locationRows) => {
        console.log(locationRows);
        return makeRequest.post("/locations/", locationRows);
      },
      onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries(["locations"]);
      },
    });
    const subjectAddMutation = useMutation({
      mutationFn: (subjectRows) => {
        return makeRequest.post("/subject/", subjectRows);
      },
      onSuccess: () => {
          // Invalidate and refetch
          queryClient.invalidateQueries(["subjects"]);
      },
    });
    const handleChange = (e) => {
      setUserData((previous)=>({...previous, [e.target.name]: e.target.value}));
      setErr("");
    }
    const handleSubmit = async (e) => {
      console.log(locations);
      console.log(subjects);
      e.preventDefault();//not workinggg ↓
      //the solution is limiting the file sizes, or doing something to prevent the code going
      //forward while we upload, only small pictures are uploaded fast enough
      if(subjects.length === 0 && locations.length === 0 && userData.nickname === "" && profilePic === null){ return setErr("Nothing to update"); }
      let profilePicUrl = user.profilePicture;
      if (profilePic) {
        try {
          profilePicUrl = await upload(e); // Wait for the upload to complete
        } catch (error) {
          console.error("Upload failed:", error);
          setErr("Failed to upload profile picture");
          return; // Exit early if the upload fails
        }
      }
      setProfilePic(null);
      mutation.mutate({...userData, profilePicture: `${profilePicUrl}`});
      if(locations.length>0)locationAddMutation.mutate({rows:locations});//add data and set it to empty
      setLocations([]);
      if(subjects.length >0)subjectAddMutation.mutate({rows:subjects});
      setSubjects([]);
    }
    useEffect(() => {
      if (err) {
          const timer = setTimeout(() => {
              setErr(''); 
          }, 3000);
  
          // Cleanup function to clear the timer
          return () => clearTimeout(timer);
      }
  }, [err]);
    return (
      <div className="update">
        <div className="updateForm">
          <div className="updateNav">
            <span className="updateOption" onClick={()=>{setUpdateLocation(false);setUpdateSubject(false);setUpdateUser(true)}}>Update General Info</span>
            <span className="updateOption" onClick={()=>{setUpdateUser(false);setUpdateSubject(false);setUpdateLocation(true)}}>Update Location</span>
            { user.role==="tutor" && <span className="updateOption" onClick={()=>{setUpdateLocation(false);setUpdateUser(false);setUpdateSubject(true)}}>Update Subject</span>}
          </div>
          <div className="updateContent" ref={contentRef}>
            <div className="updateUser">  
              {updateUser && <>
                <span style={{marginBottom:"5px"}}>Change your profile picture</span>
                <input style={{width:"fit-content"}} type="file" id = "profilePicture" onChange={(e) => setProfilePic(e.target.files[0])}></input>
                <span style={{color:"#6E5676 ", padding:"3px 0px 5px 0px"}}>Works best with low resolution/small images</span>
                <input type="text" style={{margin:"0"}} name = "nickname" value = {userData.nickname} onChange={handleChange} placeholder = "Change nickname" ></input>
              </>}
            </div>
            {updateLocation && <UpdateLocation locations={locations} setLocations={setLocations}/>}
            {updateSubject && <UpdateSubject subjects={subjects} setSubjects={setSubjects}/>}
          </div>
        </div>
        <div className="updateButtons">
          <button className = "updateProfileButton" type = "button" onClick={handleSubmit} ref={buttonRef}>Update</button>
          {err && <span style={{color:"red"}}>{err}</span>}
          <button className = "closeUpdateButton" type = "button" onClick={()=>setUpdateOpen(false)}>X</button>
        </div>
      </div>
    )
  }
  
  export default Update