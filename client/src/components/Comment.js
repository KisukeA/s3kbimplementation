import { useContext, useState, useEffect, useRef } from "react";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { AuthContext } from "../context/AuthContext.js";
import { Link } from "react-router-dom";
import { makeRequest } from "../axios.js";
import moment from "moment";
import Contract from "./Contract.js";
import "./Comment.css";

const Comment = ({ comment, postId, subjectId, postAuthor, postType }) => {

    const [ menu, setMenu ] = useState(false);
    const [ openContract, setOpenContract ] = useState(false);
    const { user } = useContext(AuthContext);
    const commentMenuRef = useRef();
    const queryClient = useQueryClient();

    const { isLoading,error, data } = useQuery({
      queryKey:["writerRole",comment.writer_id],
      queryFn: () => {
          return makeRequest.get("/users/role/"+comment.writer_id).then((res) => {
            return res.data[0];
          });
      }
    });
    const deleteMutation = useMutation({
        mutationFn: (commentId) => {
          return makeRequest.delete("/comments/" + commentId);
        },
        onSuccess: () => {
          //queryClient.invalidateQueries([`comments${postId}`]);
          queryClient.invalidateQueries([`comments`]);
        },
    });
    const handleDelete = (commentId) => {
        setMenu(false);
        deleteMutation.mutate(commentId);
    }
    const handleClickOutside = (event) => {
        if (commentMenuRef.current && !commentMenuRef.current.contains(event.target)) {
            setMenu(false);
        }
    };
    useEffect(() => {
        if (menu) {
            document.addEventListener('click', handleClickOutside, true);
        }
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    }, [menu]);
    return (
        <div key={comment.id} className="comment">
            <Link to={comment.writer_id !== user.id ? `/userprofile/` + comment.writer_id : `/profile/`+user.id}>
                <img  src={`/upload/${comment.profilePicture?comment.profilePicture:"noPP.jpg"}`}
                className="commentersProfilePic" alt="avtr" />
            </Link>
            <div className="info">
              <span>{comment.writer_id}</span>
              <p>{comment.content}</p>
            </div>
            <span className="date">
              {moment(comment.created).fromNow()}
            </span>
            {/*if its a call the user can contract every commenter except himself */}
            {/*additional constraint, he can only contract tutors if he's a student*/}
            { postAuthor===user.id && postType === "call" && comment.writer_id !== user.id &&
              (user.role==="tutor" || (user.role!==data?.role)) && 
              <span className="contractComment">
                <button onClick={() => setOpenContract(!openContract)} className="comment-contract-btn">Contract</button>
              </span>
            }
            {comment.writer_id === user.id && 
              <div onClick={()=>setMenu(true)} className="commentMenu">&#10247;
                {menu && <div className="commentDropdownDiv" ref={commentMenuRef}>
                    <span onClick = {()=>handleDelete(comment.id)}>Delete</span>
                    <span>Edit</span>
                </div>}
              </div>
            }
            {/*if user is tutor we can contract anyone, otherwise student can only offer to tutors */}
          {openContract && <Contract authorId = {comment.writer_id} subjectId={subjectId} postId={postId} openContract = {setOpenContract}/>}
        </div>
    )
  }
  
  export default Comment