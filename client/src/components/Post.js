import { AuthContext } from "../context/AuthContext.js";
//import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
//import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import React, { useContext, useEffect, useState, useRef } from "react";
import { makeRequest } from "../axios.js";
import { Link } from "react-router-dom";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import Comments from "./Comments.js";
import Contract from "./Contract.js";
import moment from "moment";
import "./Post.css";


const Post = ({ post, setNoti }) => {
  const [ comments, setOpenComments ] = useState(false);
  const [ openContract, setOpenContract ] = useState(false);
  const { user } = useContext(AuthContext);
  const [ isMenu, setIsMenu ] = useState(false);
  const [ noOfComments, setNoOfComments ] = useState(null);
  
  const postMenuRef = useRef();
  const { isLoading:roleLoading, data:roleData } = useQuery({
    queryKey:["writerRole",post.author_id],
    queryFn: () => {
        return makeRequest.get("/users/role/"+post.author_id).then((res) => {
          return res.data[0];
        });
    }
  });
  const { isLoading, data } = useQuery({
    queryKey:["likes", post.id],
    queryFn: () => {
        return makeRequest.get("/likes/" + post.id).then((res) => {
          return res.data;
        });
    }
  });
  const likesMutation = useMutation({
    mutationFn: (liked) => {
      return liked ? makeRequest.delete(`/likes/` + post.id)
      : makeRequest.post("/likes/",{postId:post.id});
    },
    onSuccess: () => {
      //setNoti(true);
      queryClient.invalidateQueries(["likes"]);
      
    },
  });
  const queryClient = useQueryClient();
  const mutation = useMutation({
    mutationFn: (postId) => {
      return makeRequest.delete("/posts/" + postId);
    },
    onSuccess: () => {
      setNoti(true);
      queryClient.invalidateQueries(["posts"]);
    },
  });
  const handleLike = () => {
    likesMutation.mutate(data.includes(user.id));
  }
  const handleDelete = () => {
    mutation.mutate(post.id);
  }
  const handleEdit = () => {}
  const handleClickOutside = (event) => {
    if (postMenuRef.current && !postMenuRef.current.contains(event.target)) {
        setIsMenu(false);
    }
  };
  useEffect(() => {
    if (isMenu) {
        document.addEventListener('click', handleClickOutside, true);
    }
    return () => {
        document.removeEventListener('click', handleClickOutside, true);
    };
  }, [isMenu]);
    return (
      <div className="post">
        <div className="post-header">
          <div className="post-profile">
            {/*if we are clicking on our post we get sent to our profile page as normal
              not as like we are seiing another users page */}
            <Link to={post.author_id !== user.id ? `/userprofile/`+post.author_id : ``} 
              style={{ textDecoration: "none" }}>
              <img src={`/upload/${post.profilePicture?post.profilePicture:"noPP.jpg"}`} alt="User Avatar" className="post-profile-picture" />
            </Link>
            <h3 className="post-author">{post.username}</h3>
            <h3 className="post-title">{post.type}</h3>
            <h3 className="post-about">({post.name})</h3>
          </div>
          <div className="post-options">
            {post.author_id === user.id && 
              <div onClick={()=>setIsMenu(true)}className="postMenu">&#10247;
                {isMenu && <div ref={postMenuRef} className="postDropdownDiv">
                          <span className="deletePostButton" onClick = {handleDelete}>Delete</span>
                          <span className="editPostButton" onClick = {handleEdit}>Edit</span>
                </div>}
              </div>
            }
          </div>
        </div>
        <div className="post-content">
          <div className="post-metadata">
          {moment(post.created).fromNow()}
          </div>
          <p className="posts-content">{post.content}</p>
        </div>
        <div className="post-footer">
          <div className="post-actions">
            <div className="post-likes-container">
              {isLoading ? "loading" : data?.includes(user.id) ? <span onClick={handleLike} style={{paddingBottom:"5px",fontSize:"30px",color:"red", cursor:"pointer"}}>&#9829;</span>
                : <span onClick={handleLike} style={{fontSize:"30px",paddingBottom:"5px",color:"black", cursor:"pointer"}}>&#9825;</span>
              }
              <label className="post-likes-label">{data?.length} {data?.length!==1 ? "likes" : "like" }</label>
            </div>
            {/*if the post is a request the user can contract the author of the post */}
            {/*additional constraint, he can only contract tutors if he's a student*/}
            { user.id !== post.author_id && post.type ==="request" && 
              (user.role==="tutor" || (user.role!==roleData?.role)) &&
            <>
              <button onClick={() => setOpenContract(!openContract)} className="post-contract">Contract</button>
            </>}
            <label className="post-comments-label" onClick={() => setOpenComments(!comments)}>{noOfComments} {noOfComments!==1?"comments":"comment"}</label>
          </div>
          <div className="post-comments">
            {comments && <Comments setNumber = {setNoOfComments} postType={post.type} postAuthor={post.author_id} postId={post.id} subjectId={post.about} />}
          </div>
          {openContract && <Contract authorId = {post.author_id} postId={post.id} subjectId={post.about} openContract = {setOpenContract}/>}
        </div>
      </div>

    )
  }
  
  export default Post