import { useContext, useState, useEffect, useRef } from "react";
import { AuthContext } from "../context/AuthContext.js";
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import { makeRequest } from "../axios.js";
import Comment from "./Comment.js";
import "./Comments.css";

const Comments = ({ postAuthor,postId, subjectId, postType,setNumber }) => {
    const { user } = useContext(AuthContext);
    const [content, setContent] = useState("");
    const queryClient = useQueryClient();
    //vars for showing overflow arrow in comments
    const [topArrow, setTopArrow] = useState(false);
    const [botArrow, setBotArrow] = useState(false);
    //reference to the comments container div
    const commentsRef = useRef(null);
    const { isLoading, error, data } = useQuery({
      //this caused a problem becuase each comments component had access to this query result 
      //so they fetched each others comments
      //that's why i distinguished it with id's
      //queryKey:[`comments${postId}`],
      queryKey:[`comments`,postId],
      queryFn: () => {
          return makeRequest.get("/comments?postId=" + postId).then((res) => {
              setNumber(res.data.length);
              return res.data;
          });
      }
    });
    const checkForOverflow = () => {
      const container = commentsRef.current;
      const isOverflowingTop = container?.scrollTop > 0;
      const isOverflowingBottom = (container?.scrollTop+10) < (container?.scrollHeight - container?.clientHeight);
      setBotArrow(isOverflowingBottom);
      setTopArrow(isOverflowingTop);
    };
    useEffect(() => {
      const container = commentsRef.current;
      container?.addEventListener('scroll', checkForOverflow);
      checkForOverflow(); // Initial check on mount
      return () => container?.removeEventListener('scroll', checkForOverflow);
    }, []);
    const mutation = useMutation({
            mutationFn: (newComment) => {
              return makeRequest.post("/comments", newComment);
            },
            onSuccess: () => {
                // Invalidate and refetch
                //queryClient.invalidateQueries([`comments${postId}`]);
                queryClient.invalidateQueries([`comments`]);
            }
    })
    const handleClick = async (e) => {
        e.preventDefault();
        if(content.length===0) return;
        mutation.mutate({ content, postId });
        setContent("");
      };
    
    return (
        <div className="comments">
            <div className="writeComment">
                <img src={`/upload/${user.profilePicture?user.profilePicture:"noPP.jpg"}`} className="commentsProfilePic" alt="avatar" />
                <input
                  style={{flexGrow:"1"}}
                  type="text"
                  placeholder="write a comment"
                  value={content}
                  onChange={(e) => setContent(e.target.value)}
                />
                <button onClick={handleClick}>Send</button>
            </div>
              {error
                ? "Something went wrong"
                : isLoading
                ? "loading"
                : data.length>0?(<div className="seeComments" ref = {commentsRef}>
                    {
                      data.map((comment) => (<Comment key={comment.id} comment={comment} postAuthor={postAuthor} postType={postType} postId={postId} subjectId={subjectId} />))
                    }
                  </div>)
                : <></>  
              }
              {topArrow && <span className="topArrow"></span>}
              {botArrow && <span className="botArrow" ></span>}
        </div>
    );
}
export default Comments;
