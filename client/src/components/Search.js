import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { useState,useEffect, useRef, useContext } from "react";
import { AuthContext } from "../context/AuthContext.js";
import { makeRequest } from "../axios.js";
import MiniPost from "./MiniPost.js";
import MiniUser from "./MiniUser.js";
import "./Search.css";

const Search = () => {

    const [ search, setSearch ] = useState("");
    const [ openSearch, setOpenSearch ] = useState(false);
    const searchRef = useRef();
    
    const { user } = useContext(AuthContext);
    const  [ searchOptions, setSearchOptions ] = useState("username");
    const { isLoading, refetch, data } = useQuery({
        queryKey:["search", searchOptions, search],
        //only execute the query if this is true (there is something to search)
        enabled: !!search ,
        queryFn: () => {
          const url = searchOptions === "username" ? `/search/users/` + search 
          : searchOptions === "keywords" ? `/search/keywords/` + search
          : `/search/subject/` + search
          return makeRequest.get(url);
          //when we retreive keywords we take the whole post as a result
          //same as user we take the whole user row from db
        }
      });
    
    const handleClickOutside = (event) => {
        if (searchRef.current && !searchRef.current.contains(event.target)) {
            setOpenSearch(false);
            setSearch('');
        }
      };
    useEffect(() => {
      if (openSearch) {
          document.addEventListener('mousedown', handleClickOutside, true);
          const sourceElement = document.querySelector('.navBar');
          const targetElement = document.querySelector('.searchField');
          var sourceHeight = sourceElement.offsetHeight;
          targetElement.style.top = sourceHeight + 'px';
      }
      return () => {
          document.removeEventListener('mousedown', handleClickOutside, true);
      };
    }, [openSearch]);
    const searchFor = () => {
        setOpenSearch(true);
        if(search === "") return;
        refetch();
    }
    return (
        <div className="searchContainer" ref={searchRef}>  
            <div className="searchDiv">
              <input type="text" value = {search} name="search" placeholder="Search.." 
              className="searchSpan" onChange={(e)=>setSearch(e.target.value)}></input>
              <i onClick={searchFor} className ="fas fa-search"></i>
            </div>
            {openSearch && 
              <div className="searchField">
                <div className = "searchOptions">
                  <div className="searchOptionContainer" onClick={()=>{setSearchOptions("username")}}>
                    <input
                      id="searchOption1"
                      className={`searchOption ${searchOptions === "username"?'clicked':''}`}
                      type="radio"
                      checked = {searchOptions === "username"}
                      onChange= {()=>{setSearchOptions("username")}}
                    ></input>
                    <label htmlFor="searchOption1" className = "searchRadioLabel">Username</label>
                  </div>
                  <div className = "searchOptionContainer" onClick={()=>{setSearchOptions("keywords")}}>
                    <input
                      id="searchOption2"
                      className={`searchOption ${searchOptions === "keywords"?'clicked':''}`}
                      type="radio"
                      checked = {searchOptions === "keywords"}
                      onChange={()=>{setSearchOptions("keywords")}}
                    ></input>
                    <label htmlFor="searchOption2" className = "searchRadioLabel">Keywords</label>
                  </div>
                  <div className="searchOptionContainer" onClick={()=>{setSearchOptions("subject")}}>
                    <input
                      id="searchOption3"
                      className={`searchOption ${searchOptions === "subject"?'clicked':''}`}
                      type="radio"
                      checked = {searchOptions === "subject"}
                      onChange= {()=>{setSearchOptions("subject")}}
                    ></input>
                    <label htmlFor="searchOption3" className = "searchRadioLabel">Subject</label>
                  </div>
                </div>
                  {isLoading ? "loading..."
                  : searchOptions === "username"?
                    <div className="minisContainer">{data?.data.map((searchedUser)=>(<MiniUser searchedUser={searchedUser} key = {searchedUser.id}/>))}</div>
                  : searchOptions === "keywords"?
                    <div className="minisContainer">{data?.data.map((post)=>(<MiniPost post={post} key = {post.id}/>))}</div>
                  : <div className="minisContainer">{data?.data.map((post)=>(<MiniPost post={post} key = {post.id}/>))}</div>
                  }
              </div>
            }
        </div>
    )
  }
  
  export default Search