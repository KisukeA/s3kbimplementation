import { useState,useContext, useEffect } from 'react';
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { AuthContext } from "../context/AuthContext.js";
import './Share.css';

const Share = ({setIsShareOpen,setShareNoti}) => {
  const { user } = useContext(AuthContext);
  const [error, setError] = useState();
  const [error1, setError1] = useState();
  const [selectedFaculty, setSelectedFaculty] = useState('');
  const [keywordInput, setKeywordInput] = useState('');
  const [postData, setPostData] = useState({
    content:"",
    keywords:[],
    about:"",
    type:""
  });
  const { error:subjectError,isLoading, data:subjectsData} = useQuery({
    queryKey:["subjects"],
    queryFn: () => {
      //we fetch our subjects, others = false
      return user.role==="tutor"?makeRequest.get("/subject/false").then((res) => {
        console.log(res.data)
        return res.data;
      }):
        makeRequest.get("/subject/all").then((res) => {
          return res.data;
        })
    }
  });
  const partitioned = subjectsData?.reduce((facultyArray, subject) => {
    if (!facultyArray[subject.faculty]) {
      facultyArray[subject.faculty] = [];
    }
    
    // Push the subject into the correct type array
    facultyArray[subject.faculty].push(subject);
    
    return facultyArray;
  }, {});
  //divide the array into arrays of seperate faculties so we can map them
  useEffect(() => {
    // Only set up a timer if there's an error
    if (error) {
        document.getElementById("shareContent").style.border = "2px solid red";
        const timer = setTimeout(() => {
            setError(''); // Clear the error message
            document.getElementById("shareContent").style.border = "none";
        }, 2000); // Set timeout for 5 seconds

        // Cleanup function to clear the timer
        return () => clearTimeout(timer);
    }
  }, [error]);
  useEffect(() => {
    if (error1) {
        document.getElementById("request").style.border = "1px solid red";
        document.getElementById("call").style.border = "1px solid red";
        const timer = setTimeout(() => {
          setError1('');
          document.getElementById("request").style.border = "none";
          document.getElementById("call").style.border = "2px solid red";
        }, 2000);
        return () => clearTimeout(timer);
    }
  }, [error1]);
  const updateData = (e) => {
    setPostData((previous)=>({...previous, [e.target.name]: e.target.value}));
    
    }
  const handleAddKeyword = () => {
      if (keywordInput && !postData.keywords.includes(keywordInput)) {
        setPostData({
            ...postData,
            keywords: [...postData.keywords, keywordInput]
          });
        setKeywordInput(''); // Clear the keyword input field
      }
    };
    
  const handleRemoveKeyword = (kw) => {
    setPostData({
      ...postData,
      keywords: postData.keywords.filter(k => k !== kw) // Remove the keyword
    });
  };
  const queryClient = useQueryClient();
  const mutation = useMutation({
    mutationFn: (newPost) => {
      return makeRequest.post("/posts", newPost);
    },
    onSuccess: (data) => {
        // Invalidate and refetch
        setPostData({
          content:"",
          keywords:[],
          about:"",
          type:""
        });
        setIsShareOpen(false);
        setShareNoti(true);//start the notification pop up when we succeed in sharing post (happens in parent)
        queryClient.invalidateQueries(["posts"]);
    },
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    if(postData.content.length === 0){
      setError("This field must not be empty");
    }
    if(postData.type.length === 0){
      setError1("Please select one option");
      return;
    }
    if(postData.about === ""){
      setError1("Please select a subject");
      return;
    }
    // Handle the submission logic, like sending the data to a backend server
    const { keywords, ...others } = postData;
    const formatKeywords = keywords.length !== 0 ? "#" + keywords.join("#") : "";
    mutation.mutate({about: others.about, type: others.type, content: others.content, keywords: formatKeywords});
  };
  const handleFacultyChange = (e) => {
    setSelectedFaculty(e.target.value);
  };
  return (
    <div className="sharePostContainer">
      <form onSubmit={handleSubmit}>
        <textarea
          id = "shareContent"  
          name="content"
          className="postInput"
          //required
          placeholder="Write..."
          onChange={updateData}
          value={postData.content}
        />
        <div style = {{marginBottom:"14px", color:"red"}}>{error && error}</div>
        <div className = "keywordsContainer">
          <div className="keywordsInput">
          <input
            className='textInput'
            id = "keywords"
            type="text"
            placeholder="Keywords (optional)"
            value={keywordInput}
            onChange={(e) => setKeywordInput(e.target.value)}
          />
          <button type="button" className='keywordsButton' onClick={handleAddKeyword}>&#43;</button>
          </div>
          <ul className="keywordsList">
          {postData.keywords.map((kw, index) => (
            <li style={{display:"flex", flexDirection:"row", justifyContent:"space-around", alignItems:"center"}} key={index}>
              {kw} <button onClick={() => handleRemoveKeyword(kw)} type="button" className='keywordsButton'>&minus;</button> 
            </li>
          ))}
          </ul>
          </div>
          <div className = "radioButtons">
          <div style={{marginRight:"30px", alignSelf:"flex-start"}}>
          <input 
            type="radio"
            id="call"
            name="type"
            value="call"
            onChange={updateData}
          />
          <label htmlFor="call">call</label>
          </div>
          <div style={{alignSelf:"flex-start", marginRight:"25px"}}>
          <input
            type="radio"
            id="request"
            name="type"
            value="request"
            onChange={updateData}
          />
          <label htmlFor="request">request</label>
          </div>
          <div className='aboutContainer'>
            {user.role==="tutor"?
              <>{subjectsData?.length!==0?(
                <select name = "about" id="about" onChange={updateData} value={postData.about} className='selectMenu'>
                  <option value='' disabled >{"Select..."}</option>
                  {isLoading?"loading":error?error:subjectsData?.map((sub)=>(< option  key = {sub.id} value={sub.id}>{sub.name}</option>))}
                </select>):"Add a subject first"}
              </>
              :<>
                {selectedFaculty && <>
                  <select name = "about" key={selectedFaculty} onChange={updateData} defaultValue=''className='selectMenu'>
                    <option value='' disabled>Select...</option>
                    {partitioned[selectedFaculty].map((sub)=>(<option  key = {sub.id} value={sub.id}>{sub.name}</option>))}
                  </select>
                </>}
                <div style={{display:"flex",flexDirection:"column"}}>
                  <label style={{margin:"0"}} htmlFor="about">About:</label>
                  <select id="about" onChange={handleFacultyChange} defaultValue='' className='selectFacultyMenu'>
                    <option value='' disabled>Choose a Faculty</option>
                    {isLoading?"loading":subjectError?subjectError:Object.keys(partitioned).map((faculty) => (
                      <option key={faculty} value={faculty}>{faculty}</option>
                    ))}
                  </select>
                </div>
              </>
            }
          </div>
        </div>
        <div style = {{marginBottom:"14px", color:"red"}}>{error1 && error1}</div>
        <button className = "sendPostButton" type="submit">Post</button>
      </form>
    </div>
  );
}
export default Share;
