import moment from "moment";
import { Link } from "react-router-dom";
import ContractView from "./ContractView.js";
import { useContext, useState, useEffect } from "react";
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import { makeRequest } from "../axios.js" ;
import { AuthContext } from "../context/AuthContext.js";
import "./MiniContract.css";

//passed if it is accepted and date is overdue irl
//pending if the current user requested and is waiting answer
//upcoming is the only ones that can be canceled/deleted
const MiniContract = ({contract,passed,pending,upcoming}) => {
  //convert the db date to our (Ljubljana, Europe) date because it is not correct before we do that
  const { user } = useContext(AuthContext);
    const cdate = new Date(contract.date)
    const year = cdate.getFullYear();
    const month = (cdate.getMonth() + 1).toString().padStart(2, '0');
    const day = cdate.getDate().toString().padStart(2, '0');
    const date = `${year}-${month}-${day}`

    const [ viewContract, setViewContract ] = useState(false);
    const [ viewRate, setViewRate ] = useState(false);
    const [ rating, setRating ] = useState('');

    //this is to set a limit on cancellation, you can cancel latest 2 days before 
    const targetMoment = moment(date+" "+contract.timeFrom, "YYYY-MM-DD HH:mm");
    const now = moment();
    const differenceInHours = targetMoment.diff(now, 'hours');

    const queryClient = useQueryClient();
    const deleteMutation = useMutation({
      mutationFn: (contractId) => {
        return makeRequest.delete("/contract/"+contractId);
      },
      onSuccess: () => {
        queryClient.invalidateQueries(["contracts"]);
        
      },
    });
    const mutation = useMutation({
      mutationFn: (contract) => {
        return makeRequest.put("/contract/", contract);
      },
      onSuccess: () => {
        //queryClient.invalidateQueries(["myContracts","contracts"]);
        queryClient.invalidateQueries(["myContracts","notiContracts"]);
      },
    });
    const handleChange = (e) =>{
      setRating(e.target.value);
    }
    const handleDelete = (id) => {
      deleteMutation.mutate(id);
      //maybe add a delete animation so its evident that something's deleted
    }
    const handleCancel = () => {
      mutation.mutate({
        status:"cancelled",
        id:contract.id,
        counter:false,
        canceller:true
      });
      //its not a counter offer, and we set status to cancelled
    }
    const handleSubmit = () => {
      if(!rating) return;
      mutation.mutate({
        status:"completed",
        id:contract.id,
        counter:false,
        rating:rating,
        subject_id:contract.subject_id
      });
    }
    return (
      <div className="MiniContract">
        <span>{!passed?moment(date.split('T')[0]+'T'+contract.timeFrom).fromNow()
        : moment(date.split('T')[0]+'T'+contract.timeTo).fromNow()
        }</span>
        <span>({new Date(date).toLocaleDateString('en-GB')})</span>
        <span>From: {contract.timeFrom.split(":").slice(0,2).join(':')}</span>
        <span>To: {contract.timeTo.split(":").slice(0,2).join(':')}</span>
        {/*the same condition as down below */}
        {/*note: if its not pending and deadline hasn't passed (!passed) it can be 3 things, accepted or incoming/waiting for user's answer or counter */}
        {/*but 2 of those things are not accepted yet, so are not incoming */}
        {!pending && !passed &&<span className="aHoverContainer twoCol">{upcoming?'With: ':'Offer from: '}  
          <Link to = {`/userprofile/${user.id!==contract.offerer_id?contract.offerer_id:contract.receiver_id}`} className="aViewProfile"> {user.id!==contract.offerer_id?contract.username:contract.receiver_username} 
          </Link>
        </span>}
        {!pending && passed &&<span className="aHoverContainer twoCol">With:  
          <Link to = {`/userprofile/${user.id!==contract.offerer_id?contract.offerer_id:contract.receiver_id}`} className="aViewProfile"> {user.id!==contract.offerer_id?contract.username:contract.receiver_username}
          </Link>
        </span>}
        {pending && <span className="aHoverContainer twoCol">Offer to:  
          <Link to = {`/userprofile/${contract.receiver_id}`} className="aViewProfile"> {contract.receiver_username} 
          </Link>
        </span>}
        <span>{contract.amount}€ per hour</span>
        {/*if the time passed we display a rating button, else if the duration is right now we do nothing */}
        {/* */}
        {!pending?//if its pending we cant do anything to it
          !passed? // if its not passed(time overdue + accepted) it can be viewed because it is !!!!(THIS MEANS THAT ITS OLD BUT ONLY FOR ACCEPTED CONTRACTS)
          //either upcoming (already accepted) or incoming (waiting for our response)
          //but if its upcoming this below can never be false otherwise it would have been passed 
          //so it only occurs for incoming contracts
            (moment(date+'T'+contract.timeFrom).isAfter(moment()))?
              <button onClick={()=>{console.log("clicked here");setViewContract(true)}} className = "viewContractButton twoCol">View</button>
              :<button onClick={()=>handleDelete(contract.id)} className = "viewContractButton red twoCol">Remove</button>
            : <button onClick={()=>{setViewRate(true)}} className = "viewContractButton twoCol">Rate</button>
            //if the incoming contract is past its date we can remove it because its not "usable"
            //otherwise, if its passed, and since we know it's accepted as it's not pending,
            //we can rate the tutorship that happened
          :<></>
        }
        {/*if its an upcoming and we cannot conduct the tutorship we  may request a cancel */}
        {/*!!!!UPCOMING MEANS ACCEPTED AND STARTING AFTER TODAY*/}
        {/*maybe will add a condition (5 hours earlier or something) */}
        {/*added ;), 2days 48 hours */}
        {upcoming && differenceInHours>48 &&<button onClick={handleCancel} className = "viewContractButton twoCol">Request Cancel</button>}
        {viewContract && <ContractView viewContract = {setViewContract} contract = {contract}/>}
        {viewRate && <div className="rateWindow">
              <input type="number" value={rating} onChange={(e)=>{handleChange(e)}} min={1} max={5}></input>
              <span onClick={handleSubmit}>Submit</span>
              <span onClick={()=>{setViewRate(false)}}>X</span>
        </div>}
      </div>
    )
  }
  
  export default MiniContract