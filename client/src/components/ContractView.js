import { Link } from "react-router-dom";
// import { AuthContext } from "../context/AuthContext";
import { useContext, useState, useEffect, useRef } from "react";
import { makeRequest } from "../axios.js" ;
import { useMutation, useQueryClient, useQuery } from "@tanstack/react-query";
import moment from "moment";
import momentz from 'moment-timezone';
import MarkerMap from "./MarkerMap.js"
import "./ContractView.css";

const ContractView = ({viewContract, contract}) => {

    //const { user } = useContext(AuthContext);
    const queryClient = useQueryClient();
    const [ error, setError ] = useState(null);
    const [ edit, setEdit ] = useState(false);
    const [ openSelectLocation, setOpenSelectLocation ] = useState(false);
    const tomorrow = new Date(new Date());
    tomorrow.setDate(tomorrow.getDate() + 1);
    const mapRef = useRef();
    const [contractData, setContractData] = useState({
      id:contract.id,
      amount:contract.amount,
      date:contract.date,
      timeFrom:contract.timeFrom,
      hours:parseInt(contract.timeTo.split(":")[0])-parseInt(contract.timeFrom.split(":")[0]),
      location_id:contract.location_id,
      receiver_id:contract.offerer_id,
      status:"counter",
      counter:true
    });
    const { error:locationError,isLoading, data:locations } = useQuery({
        queryKey:[`locations`,contract.post_id],
        queryFn: () => {
          return makeRequest.get("/locations/"+contract.receiver_id).then((res) => {
            console.log(res.data);
            return res.data;
          })
        }
      });
    const mutation = useMutation({
        mutationFn: (contract) => {
          return makeRequest.put("/contract/", contract);
        },
        onSuccess: () => {
          queryClient.invalidateQueries(["myContracts","contracts"]);
        },
    });
    const deleteMutation = useMutation({
      mutationFn: (contractId) => {
        return makeRequest.delete("/contract/"+contractId);
      },
      onSuccess: () => {
        queryClient.invalidateQueries(["contracts"]);
        
      },
    });
    const respondContract = (status) => {
        mutation.mutate({
            status:status?'accepted':'declined',
            id:contract.id,
            counter:false
        })
    }
    const handleChange = (e) =>{
        setContractData((prev) => ({...prev, [e.target.name]:e.target.value}))
    }
    const handleMapClick = (location) =>{
        setContractData((prev)=>({...prev, location_id:location}))
    }
    const handleClickOutside = (event) => {
        if (mapRef.current && !mapRef.current.contains(event.target)) {
            setOpenSelectLocation(false);
        }
    };
    useEffect(() => {
        if (openSelectLocation) {
            document.addEventListener('click', handleClickOutside, true);
        }
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
      }, [openSelectLocation]);
    const submitCounterOffer = () => {
      mutation.mutate(contractData);
      setEdit(false);
      viewContract(false);
      //deleteMutation.mutate(contract.id);
    }
    console.log(contractData)
    return (
      <div className="ContractView">
        <Link to = {`/userprofile/${contract.offerer_id}`} className="aProfile">
          <img style = {{margin:"0"}} src={`/upload/${contract.profilePicture?contract.profilePicture:"noPP.jpg"}`} 
            className="userProfilePicture" />
        </Link>
        
        <div className="contractsContainer" style={{overflowY: "auto",display:"flex",flexDirection:"column"}}>
          <span>Contract view</span>
          <span>{moment(contract.date).fromNow()}</span>
          <span>({new Date(contract.date).toLocaleDateString('en-GB')})</span>
          <span>For {contract.amount}€ per hour</span>
          <span>From: {contract.timeFrom.split(":").slice(0,2).join(':')}</span>
          <span>To: {contract.timeTo.split(":").slice(0,2).join(':')}</span>
          <span className="aHoverContainer">By:  
            <Link to = {`/userprofile/`+contract.offerer_id} className="aContractView"> {contract.username} 
            </Link>
          </span>
          <button onClick={()=>{viewContract(false)}} className = "closeViewContractButton">Close View</button>
          {/*accept/decline buttons only appear on counter offers or first time offers (pending) */}
          {(contract.status ==="pending" || contract.status ==="counter") &&<button onClick={()=>{viewContract(false);respondContract(true)}} className = "respondContractButton">Accept Contract</button>}
          {(contract.status ==="pending" || contract.status ==="counter") &&<button onClick={()=>{viewContract(false);respondContract(false)}} className = "respondViewContractButton">Decline Contract</button>}
          { contract.status !== "counter" && contract.status ==="pending" && <button onClick={()=>{setEdit(true)}} className = "respondViewContractButton">Counter Offer</button>}
          {/*counter offer button only appears on first time offers (pending) and not counter offers */}
        </div>
        {edit &&  
          <div className="editContract"style={{ display:"flex",flexDirection:"row"}}>
            <form style={{width:"min-content", display:"flex",flexDirection:"column"}}>
              Counter Offer
              {/*guest functionality is still questionable this is just a placeholder for now */}
              <label htmlFor="timeFrom">Pick a time:</label>
              <input id='timeFrom' value={contractData.timeFrom} name='timeFrom' type="time" step="1800" onChange={(e)=>{handleChange(e)}}></input>
              <label htmlFor="date">Pick a date:</label>
              <input id='date' value={momentz(contractData.date).tz("Europe/Ljubljana").format('YYYY-MM-DD')} name='date' type="date" onChange={(e)=>{handleChange(e)}} min={tomorrow.toISOString().split('T')[0]}></input>
              <label htmlFor="amount">Amount per hour(in Euros):</label>
              <input id='amount' value={contractData.amount} name='amount' type="number" onChange={(e)=>{handleChange(e)}}></input>€
              <label htmlFor="hours">How many hours:</label>
              <input id='hours' value={contractData.hours}
               name='hours' type="number" onChange={(e)=>{handleChange(e)}} min={1}></input>
              {error && <span style={{color:"red"}}>{error}</span>}
              <button type="button" className = "closeMapButton" onClick={()=>setOpenSelectLocation(true)}>Select Location</button>
            </form>
            {openSelectLocation && <MarkerMap func = {handleMapClick} mapRef = {mapRef} locations={locations}/>}
            <span>{locations.find(location => location.id === contractData.location_id).placeName}</span>
            <button onClick={()=>{setEdit(false);
              setContractData({
                id:contract.id,
                amount:contract.amount,
                date:contract.date,
                timeFrom:contract.timeFrom,
                hours:0,
                location_id:contract.location_id,
                receiver_id:contract.offerer_id,
                status:"counter",
                counter:true
              })
            }} className = "closeEditContractButton">Cancel</button>            
            <button onClick={submitCounterOffer} className = "sendEditContractButton">Offer</button> 
          </div>
        }      
      </div>
    )
  }
  
  export default ContractView